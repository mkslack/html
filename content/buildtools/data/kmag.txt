kmag (screen magnifier tool)

This package is part of the KDE applications collection.

KMag is a small utility for Linux to magnify a part of the screen.
KMag is very useful for people with visual disabilities and for
those working in the fields of image analysis, web development etc.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kauth
kcodecs
kconfig
kconfigwidgets
kcoreaddons
ki18n
kio
kservice
kwidgetsaddons
kxmlgui
qt5-base
