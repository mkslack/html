python-OpenSSL (Python wrapper module around the OpenSSL library)

High-level wrapper around a subset of the OpenSSL library, includes
* SSL.Connection objects, wrapping the methods of Python's
portable sockets 
* Callbacks written in Python
* Extensive error-handling mechanism, mirroring OpenSSL's
error codes
...  and much more ;)



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.9-i486-3
openssl >= 0.9.8k-i486-3_slack13.0 | openssl-solibs >= 0.9.8k-i486-3_slack13.0
python >= 2.6.2-i486-3
