ffmpeg2theora (convert videos to theora)

With ffmpeg2theora you can convert any file that ffmpeg can decode
to theora. Right now the settings are hardcoded into the binary.
The idea is to provide ffmpeg2theora as a binary along sites like
http://v2v.cc to enable as many people as possible to encode video
clips with the same settings.

Using   ./ffmpeg2theora clip.avi   will produce a clip.ogv.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.0.23-i486-1
bzip2 >= 1.0.5-i486-1
faac >= 1.28-i486-1slp
ffmpeg >= 0.6-i486-1slp
glibc-solibs >= 2.11.1-i486-3
lame >= 3.98.4-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libkate >= 0.3.7-i486-1slp
libogg >= 1.2.0-i486-1slp
libtheora >= 1.1.1-i486-1slp
libvorbis >= 1.3.1-i486-1slp
libvpx >= 0.9.1-i486-1slp
libxcb >= 1.6-i486-1slp
speex >= 1.2rc1-i486-1slp
x264 >= 20100605-i486-1slp
xvidcore >= 1.2.2-i486-1slp
zlib >= 1.2.3-i486-2
