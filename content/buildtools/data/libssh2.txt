libssh2 (SSH2 implementation library)

libssh2 is a library implementing the SSH2 protocol, available under
the revised BSD license.

Web site: http://www.libssh2.org/



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.13-x86_64-5_slack13.37
openssl >= 0.9.8t-x86_64-1_slack13.37 | openssl-solibs >= 0.9.8t-x86_64-1_slack13.37
zlib >= 1.2.5-x86_64-4
