kimap (job-based API for interacting with IMAP servers)

This package is part of the KDE applications collection.

kimap is a job-based API for interacting with IMAP servers.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
cyrus-sasl
gcc
gcc-g++
glibc-solibs
kcodecs
kconfig
kcoreaddons
ki18n
kio
kmime
kservice
qt5-base
