intel-opencl (intel-opencl driver for Linux)

The intel-opencl driver for Linux exposes the general-purpose,
parallel compute capabilities of Intel(R) graphics for OpenCL
applications.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
libdrm >= 2.4.75-x86_64-1slp
libpciaccess >= 0.13.4-x86_64-1slp
zlib >= 1.2.8-x86_64-1
