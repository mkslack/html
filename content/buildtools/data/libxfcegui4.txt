libxfcegui4 (deprecated gtk widgets for xfce)

Various gtk widgets for the Xfce desktop environment.
Deprecated: Don't use this for future development!



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 2.20.0-x86_64-1slp
bzip2 >= 1.0.6-x86_64-1
cairo >= 1.14.6-x86_64-1slp
expat >= 2.1.0-x86_64-1
fontconfig >= 2.12.0-x86_64-1slp
freetype >= 2.6.3-x86_64-1slp
gdk-pixbuf2 >= 2.34.0-x86_64-1slp
glade3 >= 3.8.5-x86_64-2
glib2 >= 2.48.1-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
gtk+2 >= 2.24.30-x86_64-1slp
harfbuzz >= 1.2.7-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.3-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXdamage >= 1.1.4-x86_64-1slp
libXdmcp >= 1.1.2-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXfixes >= 5.0.2-x86_64-1slp
libXrender >= 0.9.9-x86_64-1slp
libXxf86vm >= 1.1.4-x86_64-1slp
libdrm >= 2.4.68-x86_64-1slp
libffi >= 3.2.1-x86_64-1
libglade >= 2.6.4-x86_64-5
libpng >= 1.6.23-x86_64-1
libxcb >= 1.12-x86_64-1slp
libxfce4util >= 4.12.1-x86_64-1slp
libxml2 >= 2.9.4-x86_64-2
libxshmfence >= 1.2-x86_64-1slp
mesa >= 12.0.1-x86_64-1slp
pango >= 1.40.1-x86_64-1slp
pcre >= 8.39-x86_64-1
pixman >= 0.34.0-x86_64-1slp
startup-notification >= 0.12-x86_64-1slp
util-linux >= 2.27.1-x86_64-1
xcb-util >= 0.4.0-x86_64-1slp
xz >= 5.2.2-x86_64-1
zlib >= 1.2.8-x86_64-1
