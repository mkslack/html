xgamma (Alter a monitor's gamma correction through the X server)

Xgamma allows X users to query and alter the gamma correction of
a monitor via the X video mode extension.

xgamma is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
libXxf86vm
