Evolution (mail, calendar and address book suite)

Evolution is the integrated mail, calendar and address book suite
from the Evolution Team.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.0.23-i486-1
atk >= 1.32.0-i486-1slp
cairo >= 1.10.0-i486-1slp
coreutils >= 8.5-i486-3 | jdk >= 6u21-i586-1slp | jre >= 6u21-i586-1slp | module-init-tools >= 3.11.1-i486-1 | sendmail >= 8.14.4-i486-1
cxxlibs >= 6.0.13-i486-2 | gcc-g++ >= 4.4.4-i486-1
cyrus-sasl >= 2.1.23-i486-1
db44 >= 4.4.20-i486-2
dbus >= 1.2.24-i486-2
dbus-glib >= 0.86-i486-1slp
enchant >= 1.6.0-i486-1slp
evolution-data-server >= 2.32.0-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.2-i486-1slp
gcc >= 4.4.4-i486-1
gconf >= 2.32.0-i486-1slp
gdk-pixbuf >= 2.22.0-i486-1slp
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
gnome-desktop >= 2.32.0-i486-1slp
gnutls >= 2.8.6-i486-1
gstreamer >= 0.10.30-i486-1slp
gtk+2 >= 2.22.0-i486-1slp
gtkimageview >= 1.6.4-i486-1slp
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXcomposite >= 0.4.2-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.3-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.2-i486-1slp
libXfixes >= 4.0.5-i486-1slp
libXi >= 1.3.2-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.6-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libcanberra >= 0.18-i486-1slp
libdrm >= 2.4.21-i486-1slp
libgcrypt >= 1.4.5-i486-2
libgdata >= 0.6.3-i486-1slp
libgnome-keyring >= 2.32.0-i486-1slp
libgpg-error >= 1.7-i486-2
libgtkhtml3 >= 3.32.0-i486-1slp
libgweather >= 2.30.3-i486-1slp
libical >= 0.43-i486-1
libnotify >= 0.4.5-i486-2
libogg >= 1.2.0-i486-1slp
libpng >= 1.4.3-i486-1slp
libproxy >= 0.2.3-i486-1slp
libsoup >= 2.32.0-i486-1slp
libtool >= 2.2.6b-i486-2
libunique >= 1.1.2-i486-1slp
libvorbis >= 1.3.1-i486-1slp
libxcb >= 1.6-i486-1slp
libxml2 >= 2.7.6-i486-1
mesa >= 7.8.2-i486-1slp
openldap-client >= 2.4.21-i486-1
openssl >= 0.9.8n-i486-1 | openssl-solibs >= 0.9.8n-i486-1
orbit2 >= 2.14.19-i486-1slp
pango >= 1.28.3-i486-1slp
perl >= 5.10.1-i486-1
pixman >= 0.19.2-i486-1slp
sqlite >= 3.7.2-i486-1slp
startup-notification >= 0.10-i486-1
util-linux-ng >= 2.17.2-i486-1
xcb-util >= 0.3.6-i486-1
zlib >= 1.2.3-i486-2
