kross-interpreters (part of the KDE Software Compilation)

kross-interpreters is part of the KDE Software Compilation.

For more information about the KDE Software Compilation please
visit the website of the KDE project:

http://www.kde.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 4.8.2-x86_64-1
gcc-g++ >= 4.8.2-x86_64-1
glibc-solibs >= 2.17-x86_64-10_slack14.1
kdelibs >= 4.14.14-x86_64-1slp
python >= 2.7.5-x86_64-1
qt >= 4.8.7-x86_64-1slp
