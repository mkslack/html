qt5-webkit (Classes for a WebKit2 based implementation)

The QtWebKit module provides the WebView API, which allows QML
applications to render regions of dynamic web content. A WebView
component may share the screen with other QML components or
encompass the full screen as specified within the QML application.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 7.3.0-x86_64-1
gcc-g++ >= 7.3.0-x86_64-1
glib2 >= 2.54.3-x86_64-1slp
glibc-solibs >= 2.26-x86_64-3
gst-plugins-base1 >= 1.12.4-x86_64-1slp
gstreamer1 >= 1.12.4-x86_64-1slp
icu4c >= 60.2-x86_64-1
libX11 >= 1.6.5-x86_64-1slp
libXcomposite >= 0.4.4-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXrender >= 0.9.10-x86_64-1slp
libjpeg-turbo >= 1.5.3-x86_64-1
libpng >= 1.6.34-x86_64-1slp
libwebp >= 0.6.1-x86_64-1slp
libxml2 >= 2.9.7-x86_64-1
libxslt >= 1.1.32-x86_64-1
mesa >= 17.4git180119-x86_64-1slp
qt5-base >= 5.10.1-x86_64-1slp
qt5-declarative >= 5.10.1-x86_64-1slp
qt5-location >= 5.10.1-x86_64-1slp
qt5-sensors >= 5.10.1-x86_64-1slp
qt5-webchannel >= 5.10.1-x86_64-1slp
sqlite >= 3.22.0-x86_64-1slp
zlib >= 1.2.11-x86_64-1
