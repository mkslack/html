pcre2 (Perl-compatible regular expression library v2)

PCRE2 is a re-working of the original PCRE library to provide a
new and improved API.

Homepage: http://www.pcre.org/



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2 >= 1.0.6-x86_64-1
glibc-solibs >= 2.27-x86_64-1
readline >= 7.0.003-x86_64-1
zlib >= 1.2.11-x86_64-1
