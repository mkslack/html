sonnet (spell checking library for Qt)

This package is part of the KDE Frameworks 5 series.

Sonnet contains a library that provides a plugin-based spell
checking library for Qt-based applications.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
aspell
gcc
gcc-g++
glibc-solibs
hunspell
qt5-base
