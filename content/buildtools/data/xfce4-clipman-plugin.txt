xfce4-clipman-plugin (clipboard manager for Xfce4)

Clipman allows you to keep several clipboard selections in memory
which you can use to toggle. It can pull the clips from both, the
selection and the copy buffer, restore your clipboard on login and
prevent an empty clipboard.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk
cairo
gdk-pixbuf2
glib2
glibc-solibs
gtk+3
libICE
libSM
libX11
libXtst
libxfce4ui
libxfce4util
pango
xfce4-panel
xfconf
zlib
