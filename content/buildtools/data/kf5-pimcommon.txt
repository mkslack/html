kf5-pimcommon (common libraries for KDEPIM)

This package is part of the KDE applications collection.

Common libraries for KDEPIM.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
grantlee5 >= 5.1.0-x86_64-1slp
kf5-akonadi >= 16.08.1-x86_64-1slp
kf5-akonadi-contacts >= 16.08.1-x86_64-1slp
kf5-attica >= 5.24.0-x86_64-1slp
kf5-karchive >= 5.24.0-x86_64-1slp
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcontacts >= 16.08.1-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kdbusaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kimap >= 16.08.1-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kitemmodels >= 5.24.0-x86_64-1slp
kf5-kjobwidgets >= 5.24.0-x86_64-1slp
kf5-kmime >= 16.08.1-x86_64-1slp
kf5-knewstuff >= 5.24.0-x86_64-1slp
kf5-kpimtextedit >= 16.08.1-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-kwallet >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
kf5-libkdepim >= 16.08.1-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
qt5-declarative >= 5.7.0-x86_64-1slp
qt5-webchannel >= 5.7.0-x86_64-1slp
qt5-webengine >= 5.7.0-x86_64-1slp
