mozilla-firefox (Mozilla Firefox Web browser)

Safe and easy web browser from Mozilla.





Visit the Mozilla Firefox project online:
http://www.mozilla.org/



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.0.27.2-x86_64-1
atk >= 2.12.0-x86_64-1slp
bzip2 >= 1.0.6-x86_64-1
cairo >= 1.12.16-x86_64-1slp
coreutils >= 8.21-x86_64-1
cxxlibs >= 6.0.18-x86_64-1 | gcc-g++ >= 4.8.2-x86_64-1
dbus >= 1.6.12-x86_64-1
dbus-glib >= 0.102-x86_64-1slp
expat >= 2.1.0-x86_64-1
fontconfig >= 2.11.1-x86_64-1slp
freetype >= 2.5.3-x86_64-1slp
gcc >= 4.8.2-x86_64-1
gdk-pixbuf2 >= 2.30.8-x86_64-1slp
glib2 >= 2.42.0-x86_64-1slp
glibc-solibs >= 2.17-x86_64-9_slack14.1
gtk+2 >= 2.24.25-x86_64-1slp
harfbuzz >= 0.9.35-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.2-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXcomposite >= 0.4.4-x86_64-1slp
libXcursor >= 1.1.14-x86_64-1slp
libXdamage >= 1.1.4-x86_64-1slp
libXdmcp >= 1.1.1-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXfixes >= 5.0.1-x86_64-1slp
libXi >= 1.7.4-x86_64-1slp
libXinerama >= 1.1.3-x86_64-1slp
libXrandr >= 1.4.2-x86_64-1slp
libXrender >= 0.9.8-x86_64-1slp
libXt >= 1.1.4-x86_64-1slp
libffi >= 3.0.13-x86_64-2
libpng >= 1.4.12-x86_64-1
libxcb >= 1.11-x86_64-1slp
mozilla-nss >= 3.15.2-x86_64-2
pango >= 1.36.7-x86_64-1slp
pixman >= 0.32.6-x86_64-1slp
startup-notification >= 0.12-x86_64-1slp
util-linux >= 2.21.2-x86_64-6
xcb-util >= 0.4.0-x86_64-1slp
zlib >= 1.2.8-x86_64-1
