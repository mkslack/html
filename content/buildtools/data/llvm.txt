llvm (The Low Level Virtual Machine)

LLVM is a compiler infrastructure designed for compile-time,
link-time, runtime, and idle-time optimization of programs from
arbitrary programming languages. 
It currently supports compilation of C and C++ programs, using
front-ends derived from GCC 4.0.1. The compiler infrastructure
includes mirror sets of programming tools as well as libraries with
equivalent functionality.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
elfutils
gcc
gcc-g++
glibc-solibs
libedit
libffi
libxml2
lua
ncurses
python2
xz
zlib
