vorbisgain (a utility to correct the volume of Ogg Vorbis files)

VorbisGain is a utility that uses a psychoacoustic method to
correct the volume of an Ogg Vorbis file to a predefined
standardized loudness.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libogg
libvorbis
