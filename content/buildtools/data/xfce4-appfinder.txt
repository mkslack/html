xfce4-appfinder (utility for Xfce4 to find installed applications)

Xfce4 Appfinder is an useful software that permits you to find every
application in the system supporting Desktop entry format.
Appfinder can search for "lost" software, add entries to xfdesktop
menu, add launchers to the panel and obviously run the selected
software from it.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
garcon
gdk-pixbuf2
glib2
glibc-solibs
gtk+3
libxfce4ui
libxfce4util
xfconf
