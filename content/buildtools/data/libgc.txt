libgc (A garbage collector for C/C++)

The Boehm-Demers-Weiser conservative garbage collector can
be used as a garbage collecting replacement for C malloc or
C++ new. It allows you to allocate memory basically as you
normally would, without explicitly deallocating memory that
is no longer useful.

The collector automatically recycles memory when it determines
that it can no longer be otherwise accessed.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 4.7.1-x86_64-1
glibc-solibs >= 2.15-x86_64-8_slack14.0
