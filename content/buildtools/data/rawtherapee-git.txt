rawtherapee (an advanced program for developing raw photos)

RawTherapee is an advanced program for developing raw photos and for
processing non-raw photos. It is non-destructive, makes use of
OpenMP, supports all the cameras supported by dcraw and carries out
its calculations in a high precision 32bit floating point engine.

This package is build using the RawTherapee development tree.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk
atkmm
cairo
cairomm
expat
fftw
gcc
gcc-g++
gdk-pixbuf2
glib2
glibc-solibs
glibmm
gtk+3
gtkmm3
lcms2
libX11
libcanberra
libiptcdata
libjpeg-turbo
liblensfun
libpng
librsvg
libsigc++
libtiff
pango
pangomm
zlib
