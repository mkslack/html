kdeaccessibility (KDE 4.x accessibility programs)

KDE accessibility programs, including kmag (a screen magnifier),
kmousetool (clicks the mouse for you), and kmouth (a speech
synthesizer frontend).



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
alsa-lib >= 1.0.18-i486-2
attr >= 2.4.43_1-i486-1
bzip2 >= 1.0.5-i486-1
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
gamin >= 0.1.10-i486-1slp
gcc >= 4.3.3-i486-3
glib2 >= 2.22.2-i486-1slp
glibc-solibs >= 2.9-i486-3
kdelibs >= 4.3.2-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXpm >= 3.5.7-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXtst >= 1.0.3-i486-1slp
libpng >= 1.2.40-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.3-i486-1slp
phonon >= 4.3.1-i486-1slp
qt4 >= 4.5.2-i486-1slp
strigi >= 0.7.0-i486-1slp
utempter >= 1.1.4-i486-1
xz >= 4.999.8beta-i486-1
zlib >= 1.2.3-i486-2
