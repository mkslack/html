kcontacts (address book API for KDE)

This package is part of the KDE Frameworks 5 series.

kcontacts is the address book API for KDE.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kcodecs
kconfig
kcoreaddons
ki18n
qt5-base
