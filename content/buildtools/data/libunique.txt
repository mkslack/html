libunique (Create single instance applications)

LibUnique is a library for writing single instance applications,
that is applications that are run once and every further call to
the same binary either exits immediately or sends a command to the
running instance.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk
cairo
dbus
dbus-glib
gdk-pixbuf2
glib2
glibc-solibs
gtk+3
libX11
pango
zlib
