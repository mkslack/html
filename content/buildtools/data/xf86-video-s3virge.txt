xf86-video-s3virge (X.org driver for S3 ViRGE based video cards)

This driver supports the S3 ViRGE based video cards.

xf86-video-s3virge is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
