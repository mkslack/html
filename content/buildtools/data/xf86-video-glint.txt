xf86-video-glint (X.org GLINT/Permedia video driver)

This driver supports 3Dlabs and Texas Instruments GLINT/Permedia
based video cards.

xf86-video-glint is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
