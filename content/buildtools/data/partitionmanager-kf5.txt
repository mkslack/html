partitionmanager (a partition manager for KDE 5.x)

KDE Partition Manager is a potentially dangerous program for your
data. It has been tested carefully and there are currently no known
bugs that could lead to data loss, but nevertheless there is always
a chance for an error to ocurr and you might lose your data.

BACK UP YOUR DATA BEFORE USING THIS SOFTWARE!



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kcrash >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kjobwidgets >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
kpmcore >= 2.1.1-x86_64-1slp
libatasmart >= 0.19-x86_64-2
qt5-base >= 5.7.0-x86_64-1slp
util-linux >= 2.27.1-x86_64-1
