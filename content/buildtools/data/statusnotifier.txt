statusnotifier (library for system tray support)

A library to use new KDE/Plasma StatusNotifierItem via GObject.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2 >= 1.0.6-x86_64-1
cairo >= 1.14.8-x86_64-1slp
expat >= 2.1.0-x86_64-1
fontconfig >= 2.12.1-x86_64-1slp
freetype >= 2.8-x86_64-1slp
gdk-pixbuf2 >= 2.36.6-x86_64-1slp
glib2 >= 2.52.2-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
gtk+3 >= 3.22.15-x86_64-1slp
harfbuzz >= 1.4.6-x86_64-1slp
libX11 >= 1.6.5-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXdmcp >= 1.1.2-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXrender >= 0.9.10-x86_64-1slp
libffi >= 3.2.1-x86_64-1
libpng >= 1.6.29-x86_64-1slp
libxcb >= 1.12-x86_64-1slp
pango >= 1.40.6-x86_64-1slp
pcre >= 8.39-x86_64-1
pixman >= 0.34.0-x86_64-1slp
util-linux >= 2.27.1-x86_64-1
zlib >= 1.2.8-x86_64-1
