libXfont2 (X11 libXfont2 runtime library)

X.Org X11 libXfont2 runtime library.

libXfont2 is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
freetype
glibc-solibs
libfontenc
zlib
