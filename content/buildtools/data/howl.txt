howl (zeroconf implementation)

Howl is a cross-platform implementation of Zeroconf networking.
Zeroconf brings a new ease of use to IP networking Branded as
'Rendezvous' by Apple Computer, Inc.,
Zeroconf (http://www.zeroconf.org/) standardizes networking protocols
for delivering hassle-free ad-hoc networking, service discovery and
IP configuration.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
avahi >= 0.6.19-i486-1slp
dbus >= 1.0.2-i486-1slp
glibc-solibs >= 2.3.6-i486-6
