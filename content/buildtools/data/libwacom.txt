libwacom (Wacom tablet library)

ibwacom is a library to identify Wacom tablets and their model-
specific features.  It provides easy access to information such as
"is this a built-in on-screen tablet", "what is the size of this
model", etc.

Homepage:  https://github.com/linuxwacom/libwacom



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glib2
glibc-solibs
libgudev
