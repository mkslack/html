libmusicbrainz (Client for low-level access to libmusicbrainz Server)

The Musicbrainz Client Library / SDK provides low-level access
to the libmusicbrainz Server RDF interface and the TRM
Signature Server.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cxxlibs >= 6.0.9-i486-1 | gcc-g++ >= 4.2.4-i486-1
expat >= 2.0.1-i486-1
gcc >= 4.2.4-i486-1
glibc-solibs >= 2.7-i486-17
