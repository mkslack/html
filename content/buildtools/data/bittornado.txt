bittornado (BitTorrent file distribution utility)

BitTorrent is a file distribution utility which utilizes the upload
capacity of machines which are downloading a file to help distribute
it to other downloading machines.  This makes it possible to provide
large files (such as the Slackware Linux ISO images) to many people
without the usual problem of insufficient bandwidth.  The more
downloaders, the more bandwidth.  Problem solved.

Bram Cohen is the genius behind BitTorrent.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
python >= 2.6.2-i486-3
