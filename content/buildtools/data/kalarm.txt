kalarm (a personal alarm message, command and email scheduler)

This package is part of the KDE applications collection.

KAlarm is a personal alarm message, command and email scheduler.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
akonadi-contacts
akonadi-mime
gcc
gcc-g++
glibc-solibs
kalarmcal
kauth
kbookmarks
kcalendarcore
kcalutils
kcodecs
kcompletion
kconfig
kconfigwidgets
kcontacts
kcoreaddons
kcrash
kdbusaddons
kglobalaccel
kguiaddons
kholidays
ki18n
kidentitymanagement
kio
kitemmodels
kitemviews
kjobwidgets
kmailtransport
kmime
knotifications
kpimtextedit
kservice
ktextwidgets
kwallet
kwidgetsaddons
kwindowsystem
kxmlgui
libX11
libkdepim
phonon-qt5
pimcommon
qt5-base
qt5-x11extras
solid
sonnet
