fuse (File system in userspace)

FUSE makes it possible to implement a filesystem in a userspace 
program. Features include: simple yet comprehensive API, secure 
mounting by non-root users, support for 2.4 and 2.6 Linux kernels, 
multi-threaded operation. etc...



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.9-i486-3
nfs-utils >= 1.1.4-i486-1 | samba >= 3.2.15-i486-1_slack13.0 | sysvinit >= 2.86-i486-6 | util-linux-ng >= 2.14.2-i486-1
samba >= 3.2.15-i486-1_slack13.0 | util-linux-ng >= 2.14.2-i486-1
