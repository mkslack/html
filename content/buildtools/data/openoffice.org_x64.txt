OpenOffice.org (Multiplatform, open-source office suite)

OpenOffice.org is a multiplatform and multilingual office suite
and an open-source project.  Compatible with all other major
office suites, the product is free to download, use, and
distribute.  This package was built from the main binary
distribution from OOo, with modifications to support full
integration into the Slackware KDE environment.

Homepage: http://www.openoffice.org/



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-x86_64-1
attr >= 2.4.43_1-x86_64-1
bzip2 >= 1.0.5-x86_64-1
e2fsprogs >= 1.41.7-x86_64-1
expat >= 2.0.1-x86_64-1
gcc >= 4.3.3-x86_64-3
gcc-g++ >= 4.3.3-x86_64-3
glibc-solibs >= 2.9-x86_64-3
libidn >= 1.5-x86_64-1
libjpeg >= 6b-x86_64-5
libmng >= 1.0.10-x86_64-1
net-tools >= 1.60-x86_64-2 | util-linux-ng >= 2.14.2-x86_64-1
openssl >= 0.9.8k-x86_64-2 | openssl-solibs >= 0.9.8k-x86_64-2
perl >= 5.10.0-x86_64-1
python >= 2.6.2-x86_64-3
zlib >= 1.2.3-x86_64-3
