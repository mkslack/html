kf5-libkeduvocdocument (library for handling vocabulary files)

This package is part of the KDE applications collection.

A library for reading and writing vocabulary files used by KDE apps.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-karchive >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
