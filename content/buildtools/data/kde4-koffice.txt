koffice (Office productivity suite for KDE 4.x)

KOffice office productivity applications, including:
o KWord (Professional text editing)
o KSpread (Professional number cruncher/spreadsheet)
o KPresenter (Professional presentation program)
o KChart (Graphing of your abstract data)
o Karbon (Vector graphics tool)
o Kugar (Database report creation)
o and many more...



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
attr >= 2.4.43_1-i486-1
bzip2 >= 1.0.5-i486-1
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
cyrus-sasl >= 2.1.23-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
gamin >= 0.1.10-i486-1slp
gcc >= 4.3.3-i486-3
glib2 >= 2.22.2-i486-1slp
glibc-solibs >= 2.9-i486-3
ilmbase >= 1.0.1-i486-1slp
jasper >= 1.900.1-i486-1slp
kdegraphics >= 4.3.2-i486-1slp
kdelibs >= 4.3.2-i486-1slp
kdepimlibs >= 4.3.2-i486-1slp
lcms >= 1.18-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXpm >= 3.5.7-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXtst >= 1.0.3-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libdrm >= 2.4.13-i486-1slp
libexiv2 >= 0.18.1-i486-1slp
libgsf >= 1.14.15-i486-1slp
libjpeg >= 6b-i486-5
libpng >= 1.2.40-i486-1slp
libtiff >= 3.8.2-i486-3
libungif >= 4.1.4-i486-4
libwmf >= 0.2.8.4-i486-4
libwpd >= 0.8.14-i486-1slp
libwpg >= 0.1.3-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.3-i486-1slp
libxslt >= 1.1.24-i486-1slp
mesa >= 7.5.1-i486-1slp
openexr >= 1.6.1-i486-1slp
openldap-client >= 2.3.43-i486-1slp
openssl >= 0.9.8k-i486-2 | openssl-solibs >= 0.9.8k-i486-2
pcre >= 7.9-i486-1slp
phonon >= 4.3.1-i486-1slp
poppler >= 0.10.7-i486-1slp
python >= 2.6.2-i486-3
qca2 >= 2.0.2-i486-1slp
qimageblitz >= 0.0.4-i486-1slp
qt4 >= 4.5.2-i486-1slp
strigi >= 0.7.0-i486-1slp
utempter >= 1.1.4-i486-1
wv2 >= 0.2.3-i486-1
xz >= 4.999.8beta-i486-1
zlib >= 1.2.3-i486-2
