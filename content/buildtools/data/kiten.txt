kiten (Japanese reference and study aid for KDE)

This package is part of the KDE applications collection.

Kiten is a Japanese reference/study tool.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
karchive
kauth
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kcrash
ki18n
kio
knotifications
kservice
kwidgetsaddons
kxmlgui
qt5-base
