baloo (framework for searching and managing metadata)

This package is part of the KDE Plasma 5 desktop series.

Baloo contains a framework for searching and managing metadata.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kconfig
kcoreaddons
kcrash
kdbusaddons
kfilemetadata
ki18n
kidletime
kio
kservice
lmdb
qt5-base
qt5-declarative
solid
