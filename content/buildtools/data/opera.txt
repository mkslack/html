Opera (internet application suite)

Opera is a free, lightweight Qt internet application suite
containing a browser, and clients for email, RSS, and IRC.

http://www.opera.com/



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.51-i486-1
atk >= 2.14.0-i486-1slp
attica >= 0.4.2-i486-1slp
attr >= 2.4.46-i486-1
bzip2 >= 1.0.6-i486-1
cairo >= 1.12.18-i486-1slp
curl >= 7.36.0-i486-1_slack14.1
cxxlibs >= 6.0.18-i486-1 | gcc-g++ >= 4.8.2-i486-1
cyrus-sasl >= 2.1.23-i486-5
expat >= 2.1.0-i486-1
fontconfig >= 2.11.1-i486-1slp
freetype >= 2.5.5-i486-1slp
gamin >= 0.1.10-i486-5
gcc >= 4.8.2-i486-1
gdk-pixbuf2 >= 2.30.8-i486-1slp
glib2 >= 2.42.2-i486-1slp
glibc-solibs >= 2.17-i486-9_slack14.1
gst-plugins-base >= 0.10.36-i486-1slp
gstreamer >= 0.10.36-i486-1slp
gtk+2 >= 2.24.27-i486-1slp
harfbuzz >= 0.9.38-i486-1slp
kdelibs >= 4.14.3-i486-1slp
libICE >= 1.0.9-i486-1slp
libSM >= 1.2.2-i486-1slp
libX11 >= 1.6.3-i486-1slp
libXau >= 1.0.8-i486-1slp
libXcomposite >= 0.4.4-i486-1slp
libXcursor >= 1.1.14-i486-1slp
libXdamage >= 1.1.4-i486-1slp
libXdmcp >= 1.1.2-i486-1slp
libXext >= 1.3.3-i486-1slp
libXfixes >= 5.0.1-i486-1slp
libXft >= 2.3.2-i486-1slp
libXi >= 1.7.4-i486-1slp
libXinerama >= 1.1.3-i486-1slp
libXpm >= 3.5.11-i486-1slp
libXrandr >= 1.5.0-i486-1slp
libXrender >= 0.9.9-i486-1slp
libXtst >= 1.2.2-i486-1slp
libdbusmenu-qt >= 0.9.3rev140619-i486-2slp
libffi >= 3.0.13-i486-2
libidn >= 1.25-i486-2
libpng >= 1.4.12-i486-1
libxcb >= 1.11-i486-1slp
libxml2 >= 2.9.1-i486-1
openldap-client >= 2.4.31-i486-2
openssl >= 1.0.1j-i486-1_slack14.1 | openssl-solibs >= 1.0.1j-i486-1_slack14.1
orc >= 0.4.23-i486-1slp
pango >= 1.36.8-i486-1slp
pixman >= 0.32.6-i486-1slp
qca >= 2.0.3-i486-1slp
qt >= 4.8.7-i486-1slp
sqlite >= 3.8.10.2-i486-1slp
strigi >= 0.7.8-i486-1slp
udev >= 182-i486-7
util-linux >= 2.21.2-i486-6
xz >= 5.0.5-i486-1
zlib >= 1.2.8-i486-1
