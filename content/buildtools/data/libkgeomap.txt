libkgeomap (library for browsing photos on a map)

This package is part of the KDE applications collection.

libkgeomap is a library which allows you to display image positions
on a map. It provides automatic grouping of images which are close
together and modifying the positions of images using draggable
markers. It currently provides two backends which can be used to
display the maps, Google Maps and Marble.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kconfig
kcoreaddons
ki18n
kio
kservice
marble
qt5-base
qt5-declarative
qt5-location
qt5-webchannel
qt5-webengine
qt5webkit
