gnome-speech (text-to-speech library)

GNOME Speech provides a simple general API for producing
text-to-speech output.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
libbonobo >= 2.32.0-i486-1slp
orbit2 >= 2.14.19-i486-1slp
zlib >= 1.2.3-i486-2
