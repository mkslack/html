libstreams (streams library for Strigi Desktop Search)

Strigi is a fast and light desktop search engine. It can handle a
large range of file formats such as emails, office documents, media
files, and file archives. It can index files that are embedded in
other files. This means email attachments and files in zip files
are searchable as if they were normal files on your harddisk.

This package is deprecated and only used for kdelibs4.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2
gcc
gcc-g++
glibc-solibs
zlib
