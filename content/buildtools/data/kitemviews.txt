kitemviews (Qt library with additional widgets for ItemModels)

This package is part of the KDE Frameworks 5 series.

KItemViews contains a library that provides a set of item views
extending the Qt model-view framework.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
qt5-base
