kf5-libkmahjongg (shared library for kmahjongg and kshisen)

This package is part of the KDE applications collection.

libkmahjongg is a library used for loading and rendering of
Mahjongg tilesets and associated backgrounds, used by KMahjongg
and KShisen.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
qt5-svg >= 5.7.0-x86_64-1slp
