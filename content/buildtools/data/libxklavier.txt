libxklavier (XKB library)

This library eases XKB (X Keyboard Extension) related development.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.1-i486-1slp
libXi >= 1.3-i486-1slp
libxcb >= 1.3-i486-1slp
libxkbfile >= 1.0.6-i486-1slp
libxml2 >= 2.7.6-i486-1slp
xkbcomp >= 1.1.1-i486-1slp
zlib >= 1.2.3-i486-2
