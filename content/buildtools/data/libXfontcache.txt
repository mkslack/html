libXfontcache (X-TrueType font cache extension client library)

FontCache is an extension that is used by X-TrueType to cache
informations about fonts.

libXfontcache is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
libXext
