mesa (a 3-D graphics library)

Mesa is a 3-D graphics library with an API very similar to that of
another well-known 3-D graphics library. :-)  The Mesa libraries are
used by X to provide both software and hardware accelerated graphics.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
elfutils >= 0.170-x86_64-1
expat >= 2.2.5-x86_64-1
gcc >= 7.3.0-x86_64-1
gcc-g++ >= 7.3.0-x86_64-1
glibc-solibs >= 2.27-x86_64-1
libX11 >= 1.6.5-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXdamage >= 1.1.4-x86_64-1slp
libXdmcp >= 1.1.2-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXfixes >= 5.0.3-x86_64-1slp
libXv >= 1.0.11-x86_64-1slp
libXvMC >= 1.0.10-x86_64-1slp
libXxf86vm >= 1.1.4-x86_64-1slp
libdrm >= 2.4.91-x86_64-1slp
libpciaccess >= 0.14-x86_64-1slp
libunwind >= 1.2.1-x86_64-1slp
libxcb >= 1.13-x86_64-1slp
libxshmfence >= 1.3-x86_64-1slp
llvm >= 5.0.1-x86_64-1slp
wayland >= 1.14.0-x86_64-1slp
zlib >= 1.2.11-x86_64-1
