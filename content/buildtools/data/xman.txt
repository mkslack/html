xman (Manual page display program for the X Window System)

Xman is a X window, graphical manual page browser.

xman is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
libXaw
libXt
