Net::SSLeay (Perl extension for using OpenSSL)

Perl extension for using OpenSSL.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.23-x86_64-1
openssl-solibs >= 1.0.2h-x86_64-1
zlib >= 1.2.8-x86_64-1
