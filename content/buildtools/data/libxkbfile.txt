libxkbfile (X11 keyboard file manipulation library)

Libxkbfile provides an interface to read and manipulate description
files for XKB, the X11 keyboard configuration extension.

libxkbfile is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
