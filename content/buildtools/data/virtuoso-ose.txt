virtuoso (a high-performance object-relational SQL database)

Virtuoso is a high-performance object-relational SQL database. As a
database, it provides transactions, a smart SQL compiler, powerful
stored-procedure language with optional JAVA and .Net server-side-
hosting, hot-backup, SQL-99 support and more. It has all major
data-access interfaces, such as ODBC, JDBC, ADO and OLE/DB.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2 >= 1.0.6-x86_64-1
cairo >= 1.12.16-x86_64-1slp
cxxlibs >= 6.0.17-x86_64-1 | gcc-g++ >= 4.7.1-x86_64-1
cyrus-sasl >= 2.1.23-x86_64-4
djvulibre >= 3.5.25.3-x86_64-1
expat >= 2.0.1-x86_64-2
fftw >= 3.2.2-x86_64-1
fontconfig >= 2.9.0-x86_64-1slp
freetype >= 2.4.12-x86_64-1slp
gcc >= 4.7.1-x86_64-1
gdk-pixbuf2 >= 2.26.5-x86_64-1slp
glib2 >= 2.34.3-x86_64-1slp
glibc-solibs >= 2.15-x86_64-8_slack14.0
graphviz >= 2.26.3-x86_64-1slp
ilmbase >= 1.0.2-x86_64-1
imagemagick >= 6.8.6.10-x86_64-1slp
jasper >= 1.900.1-x86_64-3
lcms2 >= 2.3-x86_64-1slp
libICE >= 1.0.8-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.2-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXdmcp >= 1.1.1-x86_64-1slp
libXext >= 1.3.2-x86_64-1slp
libXrender >= 0.9.8-x86_64-1slp
libXt >= 1.1.4-x86_64-1slp
libcroco >= 0.6.5-x86_64-1slp
libffi >= 3.0.13-x86_64-1slp
libjpeg >= 8d-x86_64-1slp
libpng >= 1.4.12-x86_64-1slp
librsvg >= 2.36.3-x86_64-1slp
libtiff >= 3.9.6-x86_64-1
libxcb >= 1.9.1-x86_64-1slp
libxml2 >= 2.8.0-x86_64-2_slack14.0
openexr >= 1.7.0-x86_64-1
openldap-client >= 2.4.31-x86_64-2
openssl >= 1.0.1e-x86_64-1_slack14.0 | openssl-solibs >= 1.0.1e-x86_64-1_slack14.0
pango >= 1.30.1-x86_64-1slp
pixman >= 0.30.2-x86_64-1slp
util-linux >= 2.21.2-x86_64-5
xz >= 5.0.4-x86_64-1
zlib >= 1.2.6-x86_64-1
