xf86-video-cirrus (X.org Cirrus Logic video driver)

This driver supports Cirrus Logic based video cards.

xf86-video-cirrus is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
