kamera (Digital camera support for kde applications)

This package is part of the KDE applications collection.

Kamera is a digital camera io_slave for KDE which uses gphoto2 and
libgpio to allow access to your camera's pictures using dolphin and
the URL camera:/



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kauth
kcodecs
kconfig
kconfigwidgets
kcoreaddons
ki18n
kio
kservice
kwidgetsaddons
kxmlgui
libgphoto2
qt5-base
