kimageformats (additional image format plugins for QtGui)

This package is part of the KDE Frameworks 5 series.

KImageFormats provides additional image format plugins for QtGui. 



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
ilmbase
karchive
openexr
qt5-base
