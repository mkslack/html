cantor (front-end to mathematics and statistics packages)

This package is part of the KDE applications collection.

Cantor is a front-end to powerful mathematics and statistics
packages. Cantor integrates them into the KDE Platform and provides
a nice, worksheet-based, graphical user interface.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
analitza
attica5
gcc
gcc-g++
glibc-solibs
karchive
kauth
kbookmarks
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kcrash
ki18n
kiconthemes
kio
kitemviews
kjobwidgets
knewstuff
kparts
kpty
kservice
ktexteditor
ktextwidgets
kwidgetsaddons
kwindowsystem
kxmlgui
libspectre
poppler
python3
qt5-base
qt5-svg
qt5-xmlpatterns
solid
sonnet
syntax-highlighting
