kwebkitpart (a web browser component for KDE)

KWebKitPart is a web browser component for KDE (KPart). You can use
it for example for browsing the web in Konqueror.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kdelibs >= 4.14.29-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
qtwebkit >= 2.3.4-x86_64-1slp
