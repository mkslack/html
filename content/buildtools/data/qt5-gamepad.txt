qt5-gamepad (support for getting events from gamepad devices)

Qt is a cross-platform application and UI framework for developers
using C++ or QML, a CSS &amp; JavaScript like language. Qt Creator is the
supporting Qt IDE. Qt Cloud Services provides connected application
backend features to Qt applications.

Qt, Qt Quick and the supporting tools are developed as an open source
project governed by an inclusive meritocratic model. Qt can be used
under open source (GPL v3 and LGPL v2.1) or commercial terms.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
eudev
gcc
gcc-g++
glibc-solibs
libglvnd
qt5-base
qt5-declarative
