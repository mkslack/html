kipi-plugins (plugins extending KDE graphics applications)

This package is part of the KDE applications collection.

A collection of plugins extending the KDE graphics and
image applications



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
karchive
kauth
kbookmarks
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
ki18n
kio
kitemviews
kjobwidgets
kservice
kwidgetsaddons
kwindowsystem
kxmlgui
libkipi
qt5-base
qt5-xmlpatterns
solid
