libgravatar (a library that provides Gravatar support)

This package is part of the KDE applications collection.

A KDE PIM library that provides Gravatar support.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kauth
kcodecs
kconfig
kconfigwidgets
kcoreaddons
ki18n
kio
kservice
ktextwidgets
kwidgetsaddons
pimcommon
qt5-base
sonnet
