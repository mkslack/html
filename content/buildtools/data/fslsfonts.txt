fslsfonts (Lists fonts served by X font server)

Fslsfonts lists the fonts that match the given pattern.

fslsfonts is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libFS
