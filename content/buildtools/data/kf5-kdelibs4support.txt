kf5-kdelibs4support (Porting aid from KDELibs4)

This package is part of the KDE Frameworks 5 porting aids series.

The KDELibs4Support package contains libraries and utilities to ease
the transition from kdelibs 4 to KDE Frameworks 5.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kbookmarks >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kcrash >= 5.24.0-x86_64-1slp
kf5-kdbusaddons >= 5.24.0-x86_64-1slp
kf5-kglobalaccel >= 5.24.0-x86_64-1slp
kf5-kguiaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kitemviews >= 5.24.0-x86_64-1slp
kf5-kjobwidgets >= 5.24.0-x86_64-1slp
kf5-knotifications >= 5.24.0-x86_64-1slp
kf5-kparts >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-ktextwidgets >= 5.24.0-x86_64-1slp
kf5-kunitconversion >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kwindowsystem >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
kf5-solid >= 5.24.0-x86_64-1slp
kf5-sonnet >= 5.24.0-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.3-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
qt5-svg >= 5.7.0-x86_64-1slp
qt5-tools >= 5.7.0-x86_64-1slp
qt5-x11extras >= 5.7.0-x86_64-1slp
