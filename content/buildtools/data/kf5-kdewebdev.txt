kf5-kdewebdev (web development apps)

This package is part of the KDE applications collection.

KDEWebDev includes various apps useful for web development.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kdelibs >= 4.14.22-x86_64-1slp
kf5-kdepimlibs4 >= 4.14.10-x86_64-1slp
phonon >= 4.9.0-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
tidy-html5 >= 5.3.5git160701-x86_64-1slp
zlib >= 1.2.8-x86_64-1
