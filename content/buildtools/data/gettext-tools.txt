gettext-tools (tools to handle messages in different languages)

The GNU gettext-tools package is useful for authors and maintainers
of internationalized software, or for anyone compiling programs that
use the gettext functions. This package provides the needed tools
and library functions for the handling of messages in different
languages. Some other GNU packages use the gettext program
(included in this package) to internationalize the messages given
by shell scripts.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
attr >= 2.4.43_1-i486-1
gcc >= 4.3.3-i486-3
gcc-java >= 4.3.3-i486-3
glibc-solibs >= 2.9-i486-3
ncurses >= 5.7-i486-1
zlib >= 1.2.3-i486-2
