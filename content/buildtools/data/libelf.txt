libelf (read, modify or create ELF files)

'Libelf' lets you read, modify or create ELF files in an
architecture-independent way. The library takes care of size and
endian issues, e.g. you can process a file for SPARC processors on
an Intel based system.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.11.1-i486-3
