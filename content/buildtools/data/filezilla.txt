filezilla (a free, open source FTP, FTPS and SFTP client)

FileZilla Client is a free, open source FTP, FTPS and SFTP client.
Supports FTP, FTP over SSL/TLS (FTPS) and SSH File Transfer Protocol
(SFTP), Cross-platform, Available in many languages, Supports resume
and transfer of large files &#062;4GB, Easy to use Site Manager and
transfer queue, Drag &amp; drop support, Speed limits, Filename filters, 
Network configuration wizard.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 2.4.0-x86_64-1slp
bzip2 >= 1.0.6-x86_64-1
cairo >= 1.12.16-x86_64-1slp
cxxlibs >= 6.0.17-x86_64-1 | gcc-g++ >= 4.7.1-x86_64-1
dbus >= 1.4.20-x86_64-4_slack14.0
expat >= 2.0.1-x86_64-2
fontconfig >= 2.9.0-x86_64-1slp
freetype >= 2.4.12-x86_64-1slp
gcc >= 4.7.1-x86_64-1
gdk-pixbuf2 >= 2.26.5-x86_64-1slp
glib2 >= 2.34.3-x86_64-1slp
glibc-solibs >= 2.15-x86_64-8_slack14.0
gmp >= 5.0.5-x86_64-1
gnutls >= 3.0.32-x86_64-1slp
gtk+2 >= 2.24.20-x86_64-1slp
libICE >= 1.0.8-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.2-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXcomposite >= 0.4.4-x86_64-1slp
libXcursor >= 1.1.14-x86_64-1slp
libXdamage >= 1.1.4-x86_64-1slp
libXdmcp >= 1.1.1-x86_64-1slp
libXext >= 1.3.2-x86_64-1slp
libXfixes >= 5.0.1-x86_64-1slp
libXi >= 1.7.2-x86_64-1slp
libXinerama >= 1.1.3-x86_64-1slp
libXrandr >= 1.4.2-x86_64-1slp
libXrender >= 0.9.8-x86_64-1slp
libXxf86vm >= 1.1.3-x86_64-1slp
libffi >= 3.0.13-x86_64-1slp
libidn >= 1.25-x86_64-2
libjpeg >= 8d-x86_64-1slp
libpng >= 1.4.12-x86_64-1slp
libtiff >= 3.9.6-x86_64-1
libxcb >= 1.9.1-x86_64-1slp
nettle >= 2.6-x86_64-1slp
p11-kit >= 0.12-x86_64-1
pango >= 1.30.1-x86_64-1slp
pixman >= 0.30.2-x86_64-1slp
sqlite >= 3.8.0.2-x86_64-1slp
util-linux >= 2.21.2-x86_64-5
wxGTK2.8 >= 2.8.12-x86_64-1slp
zlib >= 1.2.6-x86_64-1
