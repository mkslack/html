qt5-lottie (player software for 2d vector graphics animations)

Qt is a cross-platform application and UI framework for developers
using C++ or QML, a CSS &amp; JavaScript like language. Qt Creator is the
supporting Qt IDE. Qt Cloud Services provides connected application
backend features to Qt applications.

Lottie is a family of player software for a certain json-based
file format for describing 2d vector graphics animations.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
libglvnd
qt5-base
qt5-declarative
