libgdiplus (GDI+ API for linux)

An Open Source implementation of the GDI+ API.

This is part of the Mono project.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cairo >= 1.8.8-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.11-i486-1slp
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXrender >= 0.9.5-i486-1slp
libexif >= 0.6.16-i486-1slp
libjpeg >= 6b-i486-5
libpng >= 1.2.42-i486-1slp
libtiff >= 3.8.2-i486-3
libungif >= 4.1.4-i486-4
libxcb >= 1.3-i486-1slp
pixman >= 0.17.6-i486-1slp
zlib >= 1.2.3-i486-2
