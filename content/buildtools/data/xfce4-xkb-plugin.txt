xfce4-xkb-plugin (XFce4 XKB Layouts panel plugin)

This plugin allows you to setup and use multiple (currently
up to 4 due to X11 protocol limitation) keyboard layouts.
You can choose the keyboard model, what key combination to
use to switch between the layouts, the actual keyboard layouts,
the way in which the current layout is being displayed (country
flag image or text) and the layout policy, which is whether to
store the layout globally (for all windows), per application or
per window.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk
cairo
garcon
gdk-pixbuf2
glib2
glibc-solibs
gtk+3
librsvg
libwnck
libxfce4ui
libxfce4util
libxklavier
pango
xfce4-panel
xfconf
zlib
