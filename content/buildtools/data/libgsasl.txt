libgsasl (GNU Simple Authentication and Security Layer)

The GNU Simple Authentication and Security Layer (SASL).



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.13-x86_64-5_slack13.37
libgcrypt >= 1.4.6-x86_64-1
libgpg-error >= 1.9-x86_64-1
libidn >= 1.19-x86_64-1
