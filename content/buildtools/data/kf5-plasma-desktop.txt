kf5-plasma-desktop (the KDE Plasma 5 desktop)

This package is part of the KDE Plasma 5 desktop series.

Plasma-Desktop contains the KDE Plasma 5 Desktop. 



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
fontconfig >= 2.12.0-x86_64-1slp
freetype >= 2.6.3-x86_64-1slp
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glib2 >= 2.48.1-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
kf5-attica >= 5.26.0-x86_64-1slp
kf5-baloo >= 5.26.0-x86_64-1slp
kf5-kactivities >= 5.26.0-x86_64-1slp
kf5-kactivities-stats >= 5.26.0-x86_64-1slp
kf5-karchive >= 5.26.0-x86_64-1slp
kf5-kauth >= 5.26.0-x86_64-1slp
kf5-kbookmarks >= 5.26.0-x86_64-1slp
kf5-kcmutils >= 5.26.0-x86_64-1slp
kf5-kcodecs >= 5.26.0-x86_64-1slp
kf5-kcompletion >= 5.26.0-x86_64-1slp
kf5-kconfig >= 5.26.0-x86_64-1slp
kf5-kconfigwidgets >= 5.26.0-x86_64-1slp
kf5-kcoreaddons >= 5.26.0-x86_64-1slp
kf5-kcrash >= 5.26.0-x86_64-1slp
kf5-kdbusaddons >= 5.26.0-x86_64-1slp
kf5-kdeclarative >= 5.26.0-x86_64-1slp
kf5-kdelibs4support >= 5.26.0-x86_64-1slp
kf5-kemoticons >= 5.26.0-x86_64-1slp
kf5-kfilemetadata >= 5.26.0-x86_64-1slp
kf5-kglobalaccel >= 5.26.0-x86_64-1slp
kf5-kguiaddons >= 5.26.0-x86_64-1slp
kf5-ki18n >= 5.26.0-x86_64-1slp
kf5-kiconthemes >= 5.26.0-x86_64-1slp
kf5-kio >= 5.26.0-x86_64-1slp
kf5-kitemmodels >= 5.26.0-x86_64-1slp
kf5-kitemviews >= 5.26.0-x86_64-1slp
kf5-kjobwidgets >= 5.26.0-x86_64-1slp
kf5-knewstuff >= 5.26.0-x86_64-1slp
kf5-knotifications >= 5.26.0-x86_64-1slp
kf5-knotifyconfig >= 5.26.0-x86_64-1slp
kf5-kpackage >= 5.26.0-x86_64-1slp
kf5-kparts >= 5.26.0-x86_64-1slp
kf5-kpeople >= 5.26.0-x86_64-1slp
kf5-krunner >= 5.26.0-x86_64-1slp
kf5-kservice >= 5.26.0-x86_64-1slp
kf5-ktextwidgets >= 5.26.0-x86_64-1slp
kf5-kunitconversion >= 5.26.0-x86_64-1slp
kf5-kwallet >= 5.26.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.26.0-x86_64-1slp
kf5-kwindowsystem >= 5.26.0-x86_64-1slp
kf5-kxmlgui >= 5.26.0-x86_64-1slp
kf5-plasma-framework >= 5.26.0-x86_64-1slp
kf5-plasma-workspace >= 5.8.0-x86_64-1slp
kf5-solid >= 5.26.0-x86_64-1slp
kf5-sonnet >= 5.26.0-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.3-x86_64-1slp
libXcursor >= 1.1.14-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXfixes >= 5.0.2-x86_64-1slp
libXft >= 2.3.2-x86_64-1slp
libXi >= 1.7.6-x86_64-1slp
libXrender >= 0.9.9-x86_64-1slp
libcanberra >= 0.30-x86_64-1slp
libxcb >= 1.12-x86_64-1slp
libxkbfile >= 1.0.9-x86_64-1slp
phonon >= 4.9.0-x86_64-1slp
pulseaudio >= 9.0-x86_64-1
qt5-base >= 5.7.0-x86_64-1slp
qt5-declarative >= 5.7.0-x86_64-1slp
qt5-svg >= 5.7.0-x86_64-1slp
qt5-x11extras >= 5.7.0-x86_64-1slp
scim >= 1.4.15-x86_64-2
xcb-util-image >= 0.4.0-x86_64-1slp
