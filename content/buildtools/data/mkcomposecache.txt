mkcomposecache (Creats global Compose cache files)

Mkcomposecache is used for creating global (system-wide) Compose
cache files. Compose cache files help with application startup
times and memory usage, especially in locales with large Compose
tables (e.g. all UTF-8 locales).

mkcomposecache is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
