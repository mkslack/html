kdeclarative (provides integration of QML and KDE frameworks)

This package is part of the KDE Frameworks 5 series.

KDeclarative contains a library that provides integration of
QML and KDE workspaces.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kcompletion
kconfig
kcoreaddons
kglobalaccel
ki18n
kiconthemes
kio
kjobwidgets
kpackage
kservice
kwidgetsaddons
kwindowsystem
libepoxy
qt5-base
qt5-declarative
