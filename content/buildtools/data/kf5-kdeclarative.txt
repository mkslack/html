kf5-kdeclarative (provides integration of QML and KDE frameworks)

This package is part of the KDE Frameworks 5 series.

KDeclarative contains a library that provides integration of
QML and KDE workspaces.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kglobalaccel >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kjobwidgets >= 5.24.0-x86_64-1slp
kf5-kpackage >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kwindowsystem >= 5.24.0-x86_64-1slp
libepoxy >= 1.3.1-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
qt5-declarative >= 5.7.0-x86_64-1slp
