xinit (start the X Window System server)

The xinit program is used to start the X Window System server and a
first client program on systems that are not using a display manager
such as xdm.
The xinit is not intended for native users. Instead, administrators
should design user-friendly scripts that present the desired
interface when starting up X. The startx script is one such example.

xinit is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
