kvpnc (KDE frontend for various VPN clients)

KVpnc is a KDE Desktop Environment frontend for various vpn clients. 
It supports Cisco VPN (vpnc), IPSec (FreeS/WAN (OpenS/WAN), racoon), 
PPTP (pptpclient) and OpenVPN.

Homepage:  http://home.gna.org/kvpnc/en/index.html



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.39_1-i486-1
attr >= 2.4.32_1-i486-1
coreutils >= 5.97-i486-1
cxxlibs >= 6.0.3-i486-1 | gcc-g++ >= 3.4.6-i486-1
expat >= 2.0.1-i486-1slp
fontconfig >= 2.4.2-i486-1slp
freetype >= 2.3.4-i486-1slp
gamin >= 0.1.8-i486-1slp
gcc >= 3.4.6-i486-1
glibc-solibs >= 2.3.6-i486-6
kdelibs >= 3.5.7-i486-1slp
libart_lgpl >= 2.3.19-i486-1slp
libgcrypt >= 1.2.4-i486-1slp
libgpg-error >= 1.5-i486-1slp
libidn >= 0.6.5-i486-1
libjpeg >= 6b-i386-4
libmng >= 1.0.5-i486-1
libpng >= 1.2.18-i486-1slp
openssl >= 0.9.8d-i486-1
procps >= 3.2.7-i486-1 | sysvinit >= 2.84-i486-69 | util-linux >= 2.12r-i486-5
qt >= 3.3.8-i486-1slp
tcpip >= 0.17-i486-39
x11 >= 6.9.0-i486-14_slack11.0
zlib >= 1.2.3-i486-1
