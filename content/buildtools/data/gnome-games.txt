gnome-games (A collection of games for GNOME)

Gnome-games is a collection of simple, but addictive, games from the
GNOME desktop project. They represent many of the popular games and
include card games, puzzle games and arcade games. They are meant to
be the sort of game that can be played in five minutes or so. They
are also meant to be fun enough that you will play them again and
again. Of course we can't be held responsible for the time and
productivity lost while playing them.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.32.0-i486-1slp
cairo >= 1.10.0-i486-1slp
clutter >= 1.2.4-i486-1slp
clutter-gtk >= 0.10.8-i486-1slp
coreutils >= 8.5-i486-3 | jdk >= 6u21-i586-1slp | jre >= 6u21-i586-1slp | module-init-tools >= 3.11.1-i486-1 | sendmail >= 8.14.4-i486-1
cxxlibs >= 6.0.13-i486-2 | gcc-g++ >= 4.4.4-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.2-i486-1slp
gcc >= 4.4.4-i486-1
gconf >= 2.32.0-i486-1slp
gdk-pixbuf >= 2.22.0-i486-1slp
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
gmp >= 5.0.1-i486-1
gtk+2 >= 2.22.0-i486-1slp
guile >= 1.8.7-i486-3
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXcomposite >= 0.4.2-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.3-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.2-i486-1slp
libXfixes >= 4.0.5-i486-1slp
libXi >= 1.3.2-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.6-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libcanberra >= 0.18-i486-1slp
libcroco >= 0.6.2-i486-1slp
libdrm >= 2.4.21-i486-1slp
libogg >= 1.2.0-i486-1slp
libpng >= 1.4.3-i486-1slp
librsvg >= 2.32.0-i486-1slp
libtool >= 2.2.6b-i486-2
libvorbis >= 1.3.1-i486-1slp
libxcb >= 1.6-i486-1slp
libxml2 >= 2.7.6-i486-1
mesa >= 7.8.2-i486-1slp
orbit2 >= 2.14.19-i486-1slp
pango >= 1.28.3-i486-1slp
pixman >= 0.19.2-i486-1slp
python >= 2.6.4-i486-1
util-linux-ng >= 2.17.2-i486-1
zlib >= 1.2.3-i486-2
