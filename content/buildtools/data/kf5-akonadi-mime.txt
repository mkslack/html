kf5-akonadi-mime (MIME integration for akonadi)

This package is part of the KDE applications collection.

Akonadi is a PIM layer, which provides an asynchronous API to access
all kind of PIM data (e.g. mails, contacts, events, todos etc.).

It consists of several processes (generally called the Akonadi
server) and a library (called client library) which encapsulates the
communication between the client and the server.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-akonadi >= 16.08.1-x86_64-1slp
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kdbusaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kitemmodels >= 5.24.0-x86_64-1slp
kf5-kmime >= 16.08.1-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
