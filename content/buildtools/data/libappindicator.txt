libappindicator (library for system tray integration)

libappindicator is an implementation of the SNI protocol targeting
GTK applications, making the SNI protocol usable by a wider range
of applications.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 2.24.0-x86_64-1slp
cairo >= 1.14.8-x86_64-1slp
fontconfig >= 2.12.1-x86_64-1slp
freetype >= 2.8-x86_64-1slp
gdk-pixbuf2 >= 2.36.6-x86_64-1slp
glib2 >= 2.52.2-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
gtk+2 >= 2.24.31-x86_64-1slp
gtk+3 >= 3.22.15-x86_64-1slp
libdbusmenu >= 12.10.2-x86_64-1slp
libindicator >= 12.10.1-x86_64-1slp
pango >= 1.40.6-x86_64-1slp
