BekeyleyDB (Perl module to access BerkeleyDB)

BerkeleyDB is a module which allows Perl programs to make use of the
facilities provided by Berkeley DB version 2 or greater. (Note: if
you want to use version 1 of Berkeley DB with Perl you need the DB_File
module).
Berkeley DB is a C library which provides a consistent interface to a
number of database formats. BerkeleyDB provides an interface to all
four of the database types (hash, btree, queue and recno) currently
supported by Berkeley DB.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
db44 >= 4.4.20-x86_64-2
glibc-solibs >= 2.13-x86_64-5_slack13.37
perl >= 5.12.3-x86_64-1
