kf5-kconfigwidgets (extra widgets for easier configuration support)

This package is part of the KDE Frameworks 5 series.

KConfigWidgets contains a library that provides widgets for
configuration dialogs.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kguiaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
