kppp (modem dialer for KDE)

This package is part of the KDE applications collection.

KPPP is used to setup PPP (Point-to-Point Protocol) connections.
This is most useful for connecting with a cell phone "modem" card
these days. It is also use to configure real modem connections.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 7.2.0-x86_64-1
gcc-g++ >= 7.2.0-x86_64-1
glibc-solibs >= 2.26-x86_64-3
kdelibs >= 4.14.38-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
