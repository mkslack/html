kontact (Personal Information Manager for KDE)

This package is part of the KDE applications collection.

The Kontact suite is the powerful PIM solution of KDE. It lets you
handle email, agenda, contacts and other 'personal' data together
in one place by delivering innovations to help you manage your
communications more easily, organize your work faster and work
together more closely, resulting in more productivity and
efficiency in digital collaboration. 



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
gcc
gcc-g++
glibc-solibs
grantleetheme
kauth
kcmutils
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kcrash
kdepim-apps-libs
ki18n
kiconthemes
kio
kitemmodels
kjobwidgets
kontactinterface
kparts
kservice
ktextwidgets
kwidgetsaddons
kwindowsystem
kxmlgui
libkdepim
qt5-base
qt5-declarative
qt5-location
qt5-webchannel
qt5-webengine
sonnet
