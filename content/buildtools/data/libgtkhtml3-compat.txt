GtkHTML (HTML rendering/printing/editing engine)

GtkHTML is a lightweight HTML rendering/printing/editing engine.  It
was originally based on KHTMLW, part of the KDE project.

This is version 3.12.3 which provides backward compatibility to
version 3.8 which is required for some applications, for example
or GnomeSharp. It can be installed together with newer versions
of libgtkhtml (evolution 2.10 requires libgtkhtml 3.14).



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.0.18-i486-2
atk >= 1.28.0-i486-1slp
audiofile >= 0.2.6-i486-1slp
avahi >= 0.6.25-i486-1slp
cairo >= 1.8.8-i486-1slp
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
dbus >= 1.2.14-i486-1slp
dbus-glib >= 0.80-i486-1slp
e2fsprogs >= 1.41.8-i486-1
esound >= 0.2.41-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.11-i486-1slp
gcc >= 4.3.3-i486-3
gconf >= 2.26.2-i486-1slp
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
gnome-keyring >= 2.28.2-i486-1slp
gnome-vfs >= 2.24.2-i486-1slp
gtk+2 >= 2.18.7-i486-1slp
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXcomposite >= 0.4.1-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.2-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.1-i486-1slp
libXfixes >= 4.0.4-i486-1slp
libXi >= 1.3-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.5-i486-1slp
libart_lgpl >= 2.3.20-i486-1slp
libbonobo >= 2.24.2-i486-1slp
libbonoboui >= 2.24.2-i486-1slp
libglade >= 2.6.4-i486-1slp
libgnome >= 2.28.0-i486-1slp
libgnomecanvas >= 2.26.0-i486-1slp
libgnomeprint >= 2.18.6-i486-1slp
libgnomeprintui >= 2.18.4-i486-1slp
libgnomeui >= 2.24.2-i486-1slp
libpng >= 1.2.42-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.6-i486-1slp
openssl >= 0.9.8k-i486-3_slack13.0 | openssl-solibs >= 0.9.8k-i486-3_slack13.0
orbit2 >= 2.14.17-i486-1slp
pango >= 1.26.2-i486-1slp
pixman >= 0.17.6-i486-1slp
popt >= 1.7-i486-2
zlib >= 1.2.3-i486-2
