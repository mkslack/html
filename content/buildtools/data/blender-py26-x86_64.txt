blender (3D content creation suite)

Blender is an open source software for 3D modeling, animation,
rendering, post-production, interactive creation and playback.

This package requires Python 2.6.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 4.3.3-x86_64-3
gcc-g++ >= 4.3.3-x86_64-3
glibc-solibs >= 2.9-x86_64-3
libjpeg >= 6b-x86_64-5
python >= 2.6.2-x86_64-3
svgalib >= 1.9.25-x86_64-2
zlib >= 1.2.3-x86_64-3
