qca-tls (SSL/TLS plugin for QCA)

This is a plugin to provide SSL/TLS capability to programs that
utilize the Qt Cryptographic Architecture (QCA).

The qca-tls plugin was written by Justin Karneges.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.11-i486-1slp
gcc >= 4.3.3-i486-3
glibc-solibs >= 2.9-i486-3
lcms >= 1.19-i486-1slp
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.2-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.1-i486-1slp
libXfixes >= 4.0.4-i486-1slp
libXft >= 2.1.14-i486-1slp
libXinerama >= 1.1-i486-1slp
libXmu >= 1.0.5-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.5-i486-1slp
libXt >= 1.0.7-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libdrm >= 2.4.18-i486-1slp
libjpeg >= 6b-i486-5
libmng >= 1.0.10-i486-1
libpng >= 1.2.42-i486-1slp
libxcb >= 1.3-i486-1slp
mesa >= 7.6.1-i486-1slp
openssl >= 0.9.8k-i486-3_slack13.0 | openssl-solibs >= 0.9.8k-i486-3_slack13.0
qt3 >= 3.3.8b-i486-1slp
zlib >= 1.2.3-i486-2
