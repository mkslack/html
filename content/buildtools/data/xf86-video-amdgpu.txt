xf86-video-amdgpu (Accelerated Open Source driver for AMDGPU cards)

amdgpu is an accelerated Open Source driver for AMDGPU cards.

xf86-video-amdgpu is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
eudev
glibc-solibs
libdrm
mesa
