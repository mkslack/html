pim-storage-service-manager (manage data storage providers)

This package is part of the KDE applications collection.

Assistant to help with managing external and cloud data
storage providers.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi >= 16.12.3-x86_64-1slp
akonadi-contacts >= 16.12.3-x86_64-1slp
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kauth >= 5.32.0-x86_64-1slp
kcodecs >= 5.32.0-x86_64-1slp
kconfig >= 5.32.0-x86_64-1slp
kconfigwidgets >= 5.32.0-x86_64-1slp
kcontacts >= 16.12.3-x86_64-1slp
kcoreaddons >= 5.32.0-x86_64-1slp
kcrash >= 5.32.0-x86_64-1slp
kdbusaddons >= 5.32.0-x86_64-1slp
ki18n >= 5.32.0-x86_64-1slp
kio >= 5.32.0-x86_64-1slp
kitemmodels >= 5.32.0-x86_64-1slp
knotifications >= 5.32.0-x86_64-1slp
knotifyconfig >= 5.32.0-x86_64-1slp
kpimtextedit >= 16.12.3-x86_64-1slp
kservice >= 5.32.0-x86_64-1slp
kwidgetsaddons >= 5.32.0-x86_64-1slp
kxmlgui >= 5.32.0-x86_64-1slp
libkdepim >= 16.12.3-x86_64-1slp
pimcommon >= 16.12.3-x86_64-1slp
qt5-base >= 5.8.0-x86_64-1slp
