libchamplain (widgets based on Clutter)

libchamplain is a Clutter based widget to display rich, eye-candy
and interactive maps.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.28.0-i486-1slp
cairo >= 1.8.8-i486-1slp
clutter >= 1.0.8-i486-1slp
clutter-gtk >= 0.10.2-i486-1slp
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
dbus >= 1.2.14-i486-1slp
dbus-glib >= 0.80-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.11-i486-1slp
gcc >= 4.3.3-i486-3
gconf >= 2.26.2-i486-1slp
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
gnome-keyring >= 2.28.2-i486-1slp
gnutls >= 2.8.5-i486-1slp
gtk+2 >= 2.18.7-i486-1slp
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXcomposite >= 0.4.1-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.2-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.1-i486-1slp
libXfixes >= 4.0.4-i486-1slp
libXi >= 1.3-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.5-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libdrm >= 2.4.18-i486-1slp
libgcrypt >= 1.4.4-i486-1slp
libgpg-error >= 1.7-i486-1slp
libpng >= 1.2.42-i486-1slp
libproxy >= 0.2.3-i486-1slp
libsoup >= 2.28.2-i486-1slp
libtasn1 >= 2.4-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.6-i486-1slp
mesa >= 7.6.1-i486-1slp
orbit2 >= 2.14.17-i486-1slp
pango >= 1.26.2-i486-1slp
pixman >= 0.17.6-i486-1slp
sqlite >= 3.6.22-i486-1slp
zlib >= 1.2.3-i486-2
