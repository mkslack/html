oxygen (Qt decoration for the Oxygen desktop theme)

This package is part of the KDE Plasma 5 desktop series.

Oxygen contains the KDE Oxygen style. 



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
frameworkintegration
gcc
gcc-g++
glibc-solibs
kauth
kcmutils
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kdecoration
kguiaddons
ki18n
kservice
kwayland
kwidgetsaddons
kwindowsystem
libxcb
qt5-base
qt5-declarative
qt5-x11extras
