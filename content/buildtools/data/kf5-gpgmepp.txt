kf5-gpgmepp (a C++ bindings wrapper for gpgme)

This package is part of the KDE applications collection.

gpgmepp is a C++ bindings wrapper for gpgme.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
gpgme >= 1.6.0-x86_64-1
libassuan >= 2.4.2-x86_64-1
libgpg-error >= 1.23-x86_64-1
qt5-base >= 5.7.0-x86_64-1slp
