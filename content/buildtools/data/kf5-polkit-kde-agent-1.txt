kf5-polkit-kde-agent-1 (KDE dialogs for PolicyKit)

This package is part of the KDE Plasma 5 desktop series.

Polkit-KDE-Agent contains a graphical Polkit authentication
agent for the KDE Plasma Desktop.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kcoreaddons >= 5.26.0-x86_64-1slp
kf5-kcrash >= 5.26.0-x86_64-1slp
kf5-kdbusaddons >= 5.26.0-x86_64-1slp
kf5-ki18n >= 5.26.0-x86_64-1slp
kf5-kiconthemes >= 5.26.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.26.0-x86_64-1slp
kf5-kwindowsystem >= 5.26.0-x86_64-1slp
polkit-qt-1 >= 0.112.0git160416-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
