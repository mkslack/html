efl (The enlightenment core libraries)

The enlightenment core libraries.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
attr >= 2.4.46-x86_64-1
bzip2 >= 1.0.6-x86_64-1
cxxlibs >= 6.0.18-x86_64-1 | gcc-g++ >= 4.8.2-x86_64-1
dbus >= 1.6.12-x86_64-1
eject >= 2.1.5-x86_64-4
expat >= 2.1.0-x86_64-1
flac >= 1.2.1-x86_64-3
fontconfig >= 2.10.93-x86_64-1
freetype >= 2.5.0.1-x86_64-1
fribidi >= 0.19.5-x86_64-1slp
giflib >= 4.1.6-x86_64-1
glib2 >= 2.36.4-x86_64-1
glibc-solibs >= 2.17-x86_64-7
gst-plugins-base1 >= 1.3.1-x86_64-1slp
gstreamer1 >= 1.3.1-x86_64-1slp
json-c >= 0.12-x86_64-1slp
libICE >= 1.0.8-x86_64-1
libSM >= 1.2.2-x86_64-1
libX11 >= 1.6.2-x86_64-1
libXScrnSaver >= 1.2.2-x86_64-1
libXau >= 1.0.8-x86_64-1
libXcomposite >= 0.4.4-x86_64-1
libXcursor >= 1.1.14-x86_64-1
libXdamage >= 1.1.4-x86_64-1
libXdmcp >= 1.1.1-x86_64-1
libXext >= 1.3.2-x86_64-1
libXfixes >= 5.0.1-x86_64-1
libXi >= 1.7.2-x86_64-1
libXinerama >= 1.1.3-x86_64-1
libXp >= 1.0.2-x86_64-1
libXrandr >= 1.4.2-x86_64-1
libXrender >= 0.9.8-x86_64-1
libXtst >= 1.2.2-x86_64-1
libXxf86vm >= 1.1.3-x86_64-1
libcap >= 2.22-x86_64-1
libdrm >= 2.4.46-x86_64-1
libffi >= 3.0.13-x86_64-2
libjpeg >= v8a-x86_64-2_slack14.1
libogg >= 1.3.0-x86_64-1
libpng >= 1.4.12-x86_64-1
libsndfile >= 1.0.25-x86_64-1
libtiff >= 3.9.7-x86_64-1
libtool >= 2.4.2-x86_64-2
libvorbis >= 1.3.3-x86_64-1slp
libxcb >= 1.9.1-x86_64-1
luajit >= 2.0.3-x86_64-1slp
mesa >= 9.1.7-x86_64-1
openjpeg >= 1.5.1-x86_64-1slp
openssl >= 1.0.1f-x86_64-1_slack14.1 | openssl-solibs >= 1.0.1f-x86_64-1_slack14.1
pulseaudio >= 5.0-x86_64-1slp
scim >= 1.4.14-x86_64-4
udev >= 182-x86_64-7
util-linux >= 2.21.2-x86_64-6
zlib >= 1.2.8-x86_64-1
