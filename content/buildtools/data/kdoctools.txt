kdoctools (Documentation generation from docbook)

This package is part of the KDE Frameworks 5 series.

KDocTools contains tools to generate documentation in various
format from DocBook files.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
karchive
libxml2
libxslt
qt5-base
