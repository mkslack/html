xfindproxy (Part of the X.Org X server)

This package is part of the Xorg X-Server.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.11.1-i486-3
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXt >= 1.0.8-i486-1slp
libxcb >= 1.6-i486-1slp
util-linux-ng >= 2.17.2-i486-1
