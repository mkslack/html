breeze (default Plasma theme)

This package is part of the KDE Plasma 5 desktop series.

Breeze contains artwork, styles and assets for the Breeze visual
style for the KDE Plasma 5.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
frameworkintegration
gcc
gcc-g++
glibc-solibs
kauth
kcmutils
kcodecs
kconfig
kconfigwidgets
kcoreaddons
kdecoration
kguiaddons
ki18n
kiconthemes
kservice
kwayland
kwidgetsaddons
kwindowsystem
libxcb
qt5-base
qt5-declarative
qt5-x11extras
