kf5-kross (Multi-language application scripting)

This package is part of the KDE Frameworks 5 porting aids series.

The Kross package contains a library that provides a scripting
bridge to embed scripting functionality into an application.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kbookmarks >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kitemviews >= 5.24.0-x86_64-1slp
kf5-kjobwidgets >= 5.24.0-x86_64-1slp
kf5-kparts >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-ktextwidgets >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
kf5-solid >= 5.24.0-x86_64-1slp
kf5-sonnet >= 5.24.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
qt5-script >= 5.7.0-x86_64-1slp
