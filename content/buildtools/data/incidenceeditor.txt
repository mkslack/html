incidenceeditor (incidenceeditor for korganizer)

This package is part of the KDE applications collection.

This is an idenceeditor for korganizer.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
akonadi-calendar
akonadi-contacts
akonadi-mime
calendarsupport
eventviews
gcc
gcc-g++
glibc-solibs
kauth
kcalendarcore
kcalutils
kcodecs
kcompletion
kconfig
kconfigwidgets
kcontacts
kcoreaddons
kdepim-apps-libs
kdiagram
ki18n
kiconthemes
kidentitymanagement
kio
kitemmodels
kjobwidgets
kldap
kmailtransport
kmime
kpimtextedit
kservice
ktextwidgets
kwallet
kwidgetsaddons
kwindowsystem
kxmlgui
libkdepim
qt5-base
sonnet
