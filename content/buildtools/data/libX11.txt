libX11 (The X11 library)

LibX11 is the main X11 library containing all the client-side code
to access the X11 windowing system.

libX11 is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libxcb
