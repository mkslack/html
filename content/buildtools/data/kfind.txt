kfind (file find utility for KDE)

This package is part of the KDE applications collection.

The Find Files tool is a useful method of searching for specific
files on your computer, or for searching for files that match a
pattern. An example of this could include searching for files of
a particular type or with certain letters in the filename, or that
contain a certain piece of text in their contents. KFind is a
graphical tool, and not normally run from the command line.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
karchive
kauth
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kfilemetadata
ki18n
kio
kjobwidgets
kservice
ktextwidgets
kwidgetsaddons
kwindowsystem
kxmlgui
qt5-base
sonnet
