kf5-kig (interactive geometry tool)

This package is part of the KDE applications collection.

Kig is an application for Interactive Geometry.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
boost >= 1.60.0-x86_64-1slp
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-karchive >= 5.24.0-x86_64-1slp
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kcompletion >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kcrash >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kio >= 5.24.0-x86_64-1slp
kf5-kjobwidgets >= 5.24.0-x86_64-1slp
kf5-kparts >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-ktexteditor >= 5.24.0-x86_64-1slp
kf5-ktextwidgets >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
kf5-sonnet >= 5.24.0-x86_64-1slp
python >= 2.7.11-x86_64-2
qt5-base >= 5.7.0-x86_64-1slp
qt5-svg >= 5.7.0-x86_64-1slp
qt5-xmlpatterns >= 5.7.0-x86_64-1slp
