libkcompactdisc4 (CD drive library for KDE 4.x)

This package is part of the KDE applications collection.

A library for interfacing with CDs.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.1.1-x86_64-2
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kdelibs >= 4.14.30-x86_64-1slp
phonon >= 4.9.1-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
