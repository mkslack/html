sdl2 (a library to write multi-media software)

The Simple DirectMedia Layer (SDL for short) is a cross-platform
library designed to make it easy to write multi-media software, such
as games and emulators.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.23-x86_64-1
mesa >= 17.0.1-x86_64-1slp
