kdepim-runtime (runtime components for Akonadi)

This package is part of the KDE applications collection.

KDEPIM-Runtime contains Akonadi agents written using KDE
Development Platform libraries. Any package that uses Akonadi
should probably pull this in as a dependency.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
akonadi-calendar
akonadi-contacts
akonadi-mime
akonadi-notes
cyrus-sasl
gcc
gcc-g++
glibc-solibs
kalarmcal
kauth
kbookmarks
kcalendarcore
kcodecs
kcompletion
kconfig
kconfigwidgets
kcontacts
kcoreaddons
kdav
kholidays
ki18n
kidentitymanagement
kimap
kio
kitemmodels
kitemviews
kjobwidgets
kmailtransport
kmbox
kmime
knotifications
knotifyconfig
kpimtextedit
kservice
ktextwidgets
kwallet
kwidgetsaddons
kwindowsystem
kxmlgui
libkgapi
pimcommon
qca
qt5-base
qt5-declarative
qt5-location
qt5-networkauth
qt5-speech
qt5-webchannel
qt5-webengine
solid
sonnet
