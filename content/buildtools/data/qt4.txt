Qt4 (a multi-platform C++ graphical user interface toolkit)

Qt is a complete and well-developed object-oriented framework for
developing graphical user interface (GUI) applications using C++.

This release is free only for development of free software for the X
Window System.  If you use Qt for developing commercial or other
non-free software, you must have a professional license.  Please see
http://www.trolltech.com/purchase.html for information on how to
obtain a professional license.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.0.23-i486-1
coreutils >= 8.5-i486-3
cups >= 1.4.3-i486-3
cxxlibs >= 6.0.13-i486-2 | gcc-g++ >= 4.4.4-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.2-i486-1slp
gcc >= 4.4.4-i486-1
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
lcms >= 1.19-i486-1
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXdamage >= 1.1.3-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.2-i486-1slp
libXfixes >= 4.0.5-i486-1slp
libXrender >= 0.9.6-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libdrm >= 2.4.21-i486-1slp
libjpeg >= 8b-i486-1slp
libmng >= 1.0.10-i486-2
libpng >= 1.4.3-i486-1slp
libtiff >= 3.9.2-i486-1
libxcb >= 1.6-i486-1slp
mesa >= 7.8.2-i486-1slp
mysql >= 5.1.50-i486-1slp
sqlite >= 3.7.2-i486-1slp
util-linux-ng >= 2.17.2-i486-1
zlib >= 1.2.3-i486-2
