kdelibs (KDE libraries)

System libraries and other resources required by KDE:  kdecore (KDE
core library), kdeui (user interface), kimgio (image formats), kfile
(file dialog), kspell (spelling checker), khtml (HTML widget), kab
(addressbook), arts (sound, mixing and animation), kstyles, kparts,
kjs (JavaScript), kio (URL fetcher), kdesu, dcop (desktop
communication program), kssl (OpenSSL integration), kinit, libkmid,
interfaces, libtldl, mimetypes, pics, and ksgmltools.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
alsa-lib >= 1.0.18-i486-2
arts >= 1.5.10-i486-1slp
aspell >= 0.60.5-i486-2
attr >= 2.4.43_1-i486-1
audiofile >= 0.2.6-i486-1slp
bzip2 >= 1.0.5-i486-1
coreutils >= 7.4-i486-1 | floppy >= 5.4-i386-3 | ogmrip >= 0.12.3-i486-1slp | shadow >= 4.0.3-i486-18 | sudo >= 1.6.8p12-i486-1 | supertux >= 0.1.3-i486-1slp
cups >= 1.3.11-i486-1slp
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
e2fsprogs >= 1.41.8-i486-1
emacs >= 22.3-i486-2 | grep >= 2.5.4-i486-1
esound >= 0.2.41-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
gamin >= 0.1.10-i486-1slp
gawk >= 3.1.6-i486-1
gcc >= 4.3.3-i486-3
glib2 >= 2.20.4-i486-1slp
glibc-solibs >= 2.9-i486-3
ilmbase >= 1.0.1-i486-1slp
jasper >= 1.900.1-i486-1slp
lcms >= 1.18-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXinerama >= 1.0.3-i486-1slp
libXmu >= 1.0.4-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXt >= 1.0.6-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libart_lgpl >= 2.3.20-i486-1slp
libdrm >= 2.4.12-i486-1slp
libidn >= 1.5-i486-1
libjpeg >= 6b-i486-5
libmad >= 0.15.1b-i486-1slp
libmng >= 1.0.10-i486-1
libogg >= 1.1.4-i486-1slp
libpng >= 1.2.38-i486-1slp
libtiff >= 3.8.2-i486-3
libvorbis >= 1.2.2-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.3-i486-1slp
libxslt >= 1.1.24-i486-1slp
mesa >= 7.5-i486-1slp
openexr >= 1.6.1-i486-1slp
openssl >= 0.9.8k-i486-2 | openssl-solibs >= 0.9.8k-i486-2
pcre >= 7.9-i486-1slp
perl >= 5.10.0-i486-1
qt3 >= 3.3.8b-i486-1slp
sudo >= 1.6.8p12-i486-1
zlib >= 1.2.3-i486-2
