xclock (An analog / digital clock for X)

The xclock program displays the time in analog or digital form. The
time is continuously updated at a frequency which may be specified
by the user.

xclock is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
libXaw
libXft
libXmu
libXrender
libXt
libxkbfile
