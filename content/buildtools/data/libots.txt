libots (A text summarizer)

The open text summarizer is an open source tool for summarizing
texts. The program reads a text and decides which sentences are
important and which are not.
The program can either print the summarized text in text format or
in HTML form where the important sentences are highlighted in red.
The program is multi lingual and work with UTF-8 code. The ots
command line tool is an example and a debug tool for the libary.
You can bind to the library from your program.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
libxml2 >= 2.7.6-i486-1slp
popt >= 1.7-i486-2
zlib >= 1.2.3-i486-2
