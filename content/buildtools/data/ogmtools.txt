OGMtools (tools to handle OGG media streams)

These tools allow information about (ogminfo) or extraction
from (ogmdemux) or creation of (ogmmerge) or the splitting of
(ogmsplit) OGG media streams. OGM is used for "OGG media streams".

Base code taken from Ogg/Vorbis CVS repository at
http://www.xiph.org/



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
libdvdread
libogg
libvorbis
