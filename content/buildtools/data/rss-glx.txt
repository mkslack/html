rss-glx (Really Slick ScreenSavers GLX Port)

The Really Slick ScreenSavers GLX Port is a port of some nifty
OpenGL screensavers that were originally written for Windows to
GLX. It is intended for use with an existing screensaver daemon
like xscreensaver.

Note: To install the RSS screensavers for the current user you
must kill the xscreensaver and the run the install script
rss-glx_install.pl



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
blender|glu
bzip2
gcc
gcc-g++
glew
glibc-solibs
imagemagick
libICE
libSM
libX11
libXext
libglvnd
