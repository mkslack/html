kwave (a sound editor for KDE)

This package is part of the KDE applications collection.

Kwave is a sound editor, it can record, play back, import and
edit many sorts of audio files including multi channel files.
KWave includes some plugins to transform audiofiles in several
ways and present a graphical view with a complete zoom and
scroll capability.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib
audiofile
fftw
flac
gcc
gcc-g++
glibc-solibs
id3lib
karchive
kauth
kbookmarks
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kcrash
kdbusaddons
ki18n
kiconthemes
kio
kitemviews
kjobwidgets
kservice
ktextwidgets
kwidgetsaddons
kwindowsystem
kxmlgui
libmad
libogg
libsamplerate
libvorbis
opus
pulseaudio
qt5-base
qt5-multimedia
solid
sonnet
zlib
