xset (a user preference utility for X)

XSet is a user preference utility for X.

xset is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libX11
libXext
libXfontcache
libXmu
libXxf86misc
