libwnck (Window Navigator Construction Kit library)

The Window Navigator Construction Kit library, used by XFCE for
handling window management tasks.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk
cairo
gdk-pixbuf2
glib2
glibc-solibs
gtk+3
libX11
libXrender
libXres
pango
startup-notification
