akonadi-import-wizard (import PIM data into Akonadi)

This package is part of the KDE applications collection.

Akonadi is a PIM layer, which provides an asynchronous API to access
all kind of PIM data (e.g. mails, contacts, events, todos etc.).

Assistant to import PIM data from other applications into Akonadi
for use in KDE PIM applications. 



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
akonadi-contacts
akonadi-mime
gcc
gcc-g++
glibc-solibs
gpgme
karchive
kauth
kcodecs
kcompletion
kconfig
kconfigwidgets
kcontacts
kcoreaddons
kcrash
kdbusaddons
ki18n
kidentitymanagement
kimap
kio
kitemmodels
kmailtransport
kmime
kpimtextedit
kservice
kwallet
kwidgetsaddons
kxmlgui
libassuan
libgpg-error
libkdepim
libkleo
mailcommon
mailimporter
messagelib
pimcommon
qt5-base
