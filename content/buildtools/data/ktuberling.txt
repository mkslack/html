ktuberling (stamp drawing toy)

This package is part of the KDE applications collection.

KTuberling a simple constructor game suitable for children and
adults alike. The idea of the game is based around a once popular
doll making concept.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kauth
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kcrash
kdbusaddons
ki18n
kio
kservice
kwidgetsaddons
kxmlgui
libkdegames
qt5-base
qt5-declarative
qt5-multimedia
qt5-svg
