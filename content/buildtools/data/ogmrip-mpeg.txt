ogmrip-mpeg (Mpeg-1 and Mpeg-2 support for OGMRip)

ogmrip-mpeg is a plugin for OGMRip which adds support for the Mpeg-1
and Mpeg-2 video codecs and the Mpeg container. It can be used to
author DVDs, VCDs and SVCDs.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.32.0-i486-1slp
cairo >= 1.10.0-i486-1slp
cxxlibs >= 6.0.13-i486-2 | gcc-g++ >= 4.4.4-i486-1
enca >= 1.12-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.2-i486-1slp
gcc >= 4.4.4-i486-1
gconf >= 2.32.0-i486-1slp
gdk-pixbuf >= 2.22.0-i486-1slp
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
gtk+2 >= 2.22.0-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXcomposite >= 0.4.2-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.3-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.2-i486-1slp
libXfixes >= 4.0.5-i486-1slp
libXi >= 1.3.2-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.6-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libdrm >= 2.4.21-i486-1slp
libdvdread >= 4.1.3-i486-1slp
libglade >= 2.6.4-i486-1slp
libpng >= 1.4.3-i486-1slp
libxcb >= 1.6-i486-1slp
libxml2 >= 2.7.6-i486-1
mesa >= 7.8.2-i486-1slp
ogmrip >= 0.13.6-i486-1slp
orbit2 >= 2.14.19-i486-1slp
pango >= 1.28.3-i486-1slp
pixman >= 0.19.2-i486-1slp
zlib >= 1.2.3-i486-2
