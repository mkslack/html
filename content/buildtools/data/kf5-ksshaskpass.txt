kf5-ksshaskpass (interactively ask for a passphrase for ssh-add)

This package is part of the KDE Plasma 5 desktop series.

Ksshaskpass provides a ssh-add helper that uses kwallet and
kpassworddialog.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kcoreaddons >= 5.26.0-x86_64-1slp
kf5-ki18n >= 5.26.0-x86_64-1slp
kf5-kwallet >= 5.26.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.26.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
