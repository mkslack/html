k3bmonkeyaudioplugin (Audio plugin for k3B)

This is an audio encoding plugin for K3b &#062;= 0.11.
It allows encoding to and decoding from the lossless
Monkey's Audio format.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
attr >= 2.4.43_1-i486-1
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
dbus >= 1.2.14-i486-1slp
dbus-qt3 >= 0.70-i486-1slp
e2fsprogs >= 1.41.8-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
gamin >= 0.1.10-i486-1slp
gcc >= 4.3.3-i486-3
glibc-solibs >= 2.9-i486-3
hal >= 0.5.11-i486-1slp
k3b >= 1.0.5-i486-1slp
kdelibs >= 3.5.10-i486-1slp
lcms >= 1.18-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXinerama >= 1.0.3-i486-1slp
libXmu >= 1.0.4-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXt >= 1.0.6-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libart_lgpl >= 2.3.20-i486-1slp
libdrm >= 2.4.12-i486-1slp
libdvdread >= 4.1.3-i486-1slp
libidn >= 1.5-i486-1
libjpeg >= 6b-i486-5
libmng >= 1.0.10-i486-1
libpng >= 1.2.38-i486-1slp
libsamplerate >= 0.1.7-i486-1slp
libxcb >= 1.3-i486-1slp
mesa >= 7.5-i486-1slp
qt3 >= 3.3.8b-i486-1slp
zlib >= 1.2.3-i486-2
