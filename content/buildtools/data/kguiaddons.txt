kguiaddons (additional addons for QtGui)

This package is part of the KDE Frameworks 5 series.

KGuiAddons contains a library that provides utilities for graphical
user interfaces in the areas of colors, fonts, text, images and
keyboard input. 



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
libICE
libSM
libX11
libXext
libxcb
qt5-base
qt5-x11extras
