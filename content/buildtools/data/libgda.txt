libgda (a library for writing gnome database programs)

libgda is a library that eases the task of writing
gnome database programs.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.32.0-i486-1slp
cairo >= 1.10.0-i486-1slp
cxxlibs >= 6.0.13-i486-2 | gcc-g++ >= 4.4.4-i486-1
dbus >= 1.2.24-i486-2
dbus-glib >= 0.86-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.2-i486-1slp
gcc >= 4.4.4-i486-1
gdk-pixbuf >= 2.22.0-i486-1slp
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
gnutls >= 2.8.6-i486-1
gtk+2 >= 2.22.0-i486-1slp
gtksourceview2 >= 2.10.5-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXcomposite >= 0.4.2-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.3-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.2-i486-1slp
libXfixes >= 4.0.5-i486-1slp
libXi >= 1.3.2-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.6-i486-1slp
libXxf86vm >= 1.1.0-i486-1slp
libdrm >= 2.4.21-i486-1slp
libgcrypt >= 1.4.5-i486-2
libgnome-keyring >= 2.32.0-i486-1slp
libgpg-error >= 1.7-i486-2
libpng >= 1.4.3-i486-1slp
libsoup >= 2.32.0-i486-1slp
libunique >= 1.1.2-i486-1slp
libxcb >= 1.6-i486-1slp
libxml2 >= 2.7.6-i486-1
libxslt >= 1.1.26-i486-1
mesa >= 7.8.2-i486-1slp
mysql >= 5.1.50-i486-1slp
ncurses >= 5.7-i486-1
openssl >= 0.9.8n-i486-1 | openssl-solibs >= 0.9.8n-i486-1
pango >= 1.28.3-i486-1slp
pixman >= 0.19.2-i486-1slp
python >= 2.6.4-i486-1
readline >= 5.2-i486-4
zlib >= 1.2.3-i486-2
