kdevplatform (KDE development platform)

This KDE module provides libraries used by kdevelop.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
apr >= 1.5.2-x86_64-1
apr-util >= 1.5.4-x86_64-2
attica5 >= 5.34.0-x86_64-1slp
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
grantlee5 >= 5.1.0-x86_64-1slp
karchive >= 5.34.0-x86_64-1slp
kauth >= 5.34.0-x86_64-1slp
kbookmarks >= 5.34.0-x86_64-1slp
kcmutils >= 5.34.0-x86_64-1slp
kcodecs >= 5.34.0-x86_64-1slp
kcompletion >= 5.34.0-x86_64-1slp
kconfig >= 5.34.0-x86_64-1slp
kconfigwidgets >= 5.34.0-x86_64-1slp
kcoreaddons >= 5.34.0-x86_64-1slp
kdeclarative >= 5.34.0-x86_64-1slp
kguiaddons >= 5.34.0-x86_64-1slp
ki18n >= 5.34.0-x86_64-1slp
kiconthemes >= 5.34.0-x86_64-1slp
kio >= 5.34.0-x86_64-1slp
kitemmodels >= 5.34.0-x86_64-1slp
kitemviews >= 5.34.0-x86_64-1slp
kjobwidgets >= 5.34.0-x86_64-1slp
knewstuff >= 5.34.0-x86_64-1slp
knotifications >= 5.34.0-x86_64-1slp
knotifyconfig >= 5.34.0-x86_64-1slp
kpackage >= 5.34.0-x86_64-1slp
kparts >= 5.34.0-x86_64-1slp
kservice >= 5.34.0-x86_64-1slp
ktexteditor >= 5.34.0-x86_64-1slp
ktextwidgets >= 5.34.0-x86_64-1slp
kwidgetsaddons >= 5.34.0-x86_64-1slp
kwindowsystem >= 5.34.0-x86_64-1slp
kxmlgui >= 5.34.0-x86_64-1slp
libkomparediff2 >= 17.04.1-x86_64-1slp
qt5-base >= 5.8.0-x86_64-1slp
qt5-declarative >= 5.8.0-x86_64-1slp
qt5-webkit >= 5.8.0-x86_64-1slp
solid >= 5.34.0-x86_64-1slp
sonnet >= 5.34.0-x86_64-1slp
subversion >= 1.9.4-x86_64-1
threadweaver >= 5.34.0-x86_64-1slp
