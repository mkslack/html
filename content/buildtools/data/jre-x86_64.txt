Java 2 RE (Java Platform SE Runtime Environment, Version)

The J2SE(TM) Runtime Environment (JRE) is intended for software
developers and vendors to redistribute with their applications.
It contains the Java virtual machine, runtime class libraries, and
Java application launcher that are necessary to run programs written
in the Java programming language.


Download source package here: http://java.sun.com/javase/downloads/



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
alsa-lib >= 1.0.18-x86_64-2
e2fsprogs >= 1.41.8-x86_64-1
glibc-solibs >= 2.9-x86_64-3
libICE >= 1.0.5-x86_64-1slp
libSM >= 1.1.0-x86_64-1slp
libX11 >= 1.2.2-x86_64-1slp
libXau >= 1.0.4-x86_64-1slp
libXdmcp >= 1.0.2-x86_64-1slp
libXext >= 1.0.5-x86_64-1slp
libXi >= 1.2.1-x86_64-1slp
libXt >= 1.0.6-x86_64-1slp
libXtst >= 1.0.3-x86_64-1slp
libxcb >= 1.3-x86_64-1slp
