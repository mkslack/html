kdegraphics (KDE graphics programs)

Graphics programs for KDE, including:  kdvi (displays TeX's device
independent .dvi files), kfax (displays fax files), kfract (a fractal
generator), kghostview (displays postscript .ps files), kpaint (a
simple drawing program), kruler (an onscreen ruler), ksnapshot (a
screenshot utility), and kview (displays many graphic file formats).



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
attr >= 2.4.43_1-i486-1
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
e2fsprogs >= 1.41.8-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
fribidi >= 0.10.9-i486-2
gamin >= 0.1.10-i486-1slp
gcc >= 4.3.3-i486-3
glibc-solibs >= 2.9-i486-3
ilmbase >= 1.0.1-i486-1slp
imlib >= 1.9.15-i486-5
kdelibs >= 3.5.10-i486-1slp
lcms >= 1.18-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXi >= 1.2.1-i486-1slp
libXinerama >= 1.0.3-i486-1slp
libXmu >= 1.0.4-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXt >= 1.0.6-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libart_lgpl >= 2.3.20-i486-1slp
libdrm >= 2.4.12-i486-1slp
libexif >= 0.6.16-i486-1slp
libgphoto2 >= 2.4.6-i486-1slp
libidn >= 1.5-i486-1
libieee1284 >= 0.2.11-i486-2
libjpeg >= 6b-i486-5
libmng >= 1.0.10-i486-1
libpng >= 1.2.38-i486-1slp
libtiff >= 3.8.2-i486-3
libtool >= 1.5.26-i486-1
libungif >= 4.1.4-i486-4
libusb >= 0.1.12-i486-2
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.3-i486-1slp
mesa >= 7.5-i486-1slp
openexr >= 1.6.1-i486-1slp
pcre >= 7.9-i486-1slp
perl >= 5.10.0-i486-1
poppler >= 0.10.7-i486-1slp
qt3 >= 3.3.8b-i486-1slp
sane >= 1.0.19-i486-3
zlib >= 1.2.3-i486-2
