python-appindicator (library for system tray integration)

A library and indicator to take menus from applications and place
them in the panel.

This package provides Python bindings so that you can use
libappindicator from a Python program.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 2.24.0-x86_64-1slp
cairo >= 1.14.8-x86_64-1slp
dbus >= 1.11.12-x86_64-1slp
dbus-glib >= 0.108git160909-x86_64-1slp
gdk-pixbuf2 >= 2.36.6-x86_64-1slp
glib2 >= 2.52.2-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
gtk+3 >= 3.22.15-x86_64-1slp
json-glib >= 1.2.8-x86_64-1slp
libappindicator >= 12.10.0-x86_64-1slp
libdbusmenu >= 12.10.2-x86_64-1slp
libindicator >= 12.10.1-x86_64-1slp
pango >= 1.40.6-x86_64-1slp
