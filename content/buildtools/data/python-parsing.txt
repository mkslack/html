python-parsing (a Python Parsing Module)

The pyparsing module is an alternative approach to creating and
executing simple grammars, vs. the traditional lex/yacc approach,
or the use of regular expressions. The pyparsing module provides a
library of classes that client code uses to construct the grammar
directly in Python code.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
(No dependicies found)
