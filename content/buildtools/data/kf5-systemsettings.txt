kf5-systemsettings (system settings interface)

This package is part of the KDE Plasma 5 desktop series.

SystemSettings contains an application used to change
KDE system settings.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kauth >= 5.26.0-x86_64-1slp
kf5-kcmutils >= 5.26.0-x86_64-1slp
kf5-kcodecs >= 5.26.0-x86_64-1slp
kf5-kcompletion >= 5.26.0-x86_64-1slp
kf5-kconfig >= 5.26.0-x86_64-1slp
kf5-kconfigwidgets >= 5.26.0-x86_64-1slp
kf5-kcoreaddons >= 5.26.0-x86_64-1slp
kf5-kdbusaddons >= 5.26.0-x86_64-1slp
kf5-khtml >= 5.26.0-x86_64-1slp
kf5-ki18n >= 5.26.0-x86_64-1slp
kf5-kiconthemes >= 5.26.0-x86_64-1slp
kf5-kio >= 5.26.0-x86_64-1slp
kf5-kitemviews >= 5.26.0-x86_64-1slp
kf5-kjobwidgets >= 5.26.0-x86_64-1slp
kf5-kjs >= 5.26.0-x86_64-1slp
kf5-kparts >= 5.26.0-x86_64-1slp
kf5-kservice >= 5.26.0-x86_64-1slp
kf5-ktextwidgets >= 5.26.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.26.0-x86_64-1slp
kf5-kwindowsystem >= 5.26.0-x86_64-1slp
kf5-kxmlgui >= 5.26.0-x86_64-1slp
kf5-sonnet >= 5.26.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
