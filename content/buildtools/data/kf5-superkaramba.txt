kf5-superkaramba (SuperKaramba theme support for Plasma)

This package is part of the KDE applications collection.

SuperKaramba is a tool that allows you to easily create interactive
widgets on your KDE desktop. Such modules are interactive programs
written in Python, Ruby or KDE JavaScript that are usually embedded
directly into the background and do not disturb the normal view of
the desktop. (Superkaramba is deprecated in favor of Plasma widgets)



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.51-x86_64-1
attica >= 0.4.2-x86_64-1slp
attr >= 2.4.46-x86_64-1
bzip2 >= 1.0.6-x86_64-1
cxxlibs >= 6.0.18-x86_64-1 | gcc-g++ >= 4.8.2-x86_64-1
expat >= 2.1.0-x86_64-1
fontconfig >= 2.11.1-x86_64-1slp
freetype >= 2.6.1-x86_64-1slp
gamin >= 0.1.10-x86_64-5
gcc >= 4.8.2-x86_64-1
glib2 >= 2.46.1-x86_64-1slp
glibc-solibs >= 2.17-x86_64-10_slack14.1
gst-plugins-base1 >= 1.6.0-x86_64-1slp
gstreamer1 >= 1.6.0-x86_64-1slp
kf5-kdelibs >= 4.14.13-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.3-x86_64-1slp
libXScrnSaver >= 1.2.2-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXcursor >= 1.1.14-x86_64-1slp
libXdamage >= 1.1.4-x86_64-1slp
libXdmcp >= 1.1.2-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXfixes >= 5.0.1-x86_64-1slp
libXft >= 2.3.2-x86_64-1slp
libXi >= 1.7.5-x86_64-1slp
libXpm >= 3.5.11-x86_64-1slp
libXrender >= 0.9.9-x86_64-1slp
libXtst >= 1.2.2-x86_64-1slp
libXxf86vm >= 1.1.4-x86_64-1slp
libdbusmenu-qt >= 0.9.3rev140619-x86_64-2slp
libdrm >= 2.4.65-x86_64-1slp
libffi >= 3.0.13-x86_64-2
libjpeg >= v8a-x86_64-2_slack14.1
libpng >= 1.4.12-x86_64-1
libxcb >= 1.11.1-x86_64-1slp
libxml2 >= 2.9.1-x86_64-1
libxshmfence >= 1.2-x86_64-1slp
libxslt >= 1.1.28-x86_64-1
mesa >= 11.0git151027-x86_64-1slp
orc >= 0.4.24-x86_64-1slp
phonon >= 4.8.3-x86_64-1slp
python >= 2.7.5-x86_64-1
qca >= 2.0.3-x86_64-1slp
qimageblitz >= 0.0.6-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
qtwebkit >= 2.3.4-x86_64-1slp
sqlite >= 3.8.11.1-x86_64-1slp
strigi >= 0.7.8-x86_64-1slp
udev >= 182-x86_64-7
utempter >= 1.1.5-x86_64-1
util-linux >= 2.21.2-x86_64-6
xz >= 5.0.5-x86_64-1
zlib >= 1.2.8-x86_64-1
