at-spi2-core (Protocol definitions and daemon for D-Bus at-spi)

at-spi allows assistive technologies to access GTK-based
applications. Essentially it exposes the internals of applications
for automation, so tools such as screen readers, magnifiers, or even
scripting interfaces can query and interact with GUI controls.

This version of at-spi is a major break from previous versions.
It has been completely rewritten to use D-Bus rather than
ORBIT / CORBA for its transport protocol.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
dbus
glib2
glibc-solibs
libX11
libXtst
