compiz-fusion-plugins-extra (Extra plugins for Compiz Fusion)

Collection of extra plugins for Compiz Fusion.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.24.0-i486-1slp
cairo >= 1.8.4-i486-1slp
cxxlibs >= 6.0.9-i486-1 | gcc-g++ >= 4.2.4-i486-1
dbus >= 1.2.10-i486-1slp
dbus-glib >= 0.78-i486-1slp
e2fsprogs >= 1.41.3-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.7-i486-1slp
gcc >= 4.2.4-i486-1
glib2 >= 2.18.3-i486-1slp
glibc-solibs >= 2.7-i486-17
glitz >= 0.5.6-i486-1slp
gtk+2 >= 2.14.6-i486-1slp
libICE >= 1.0.4-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.1.5-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcomposite >= 0.4.0-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.4-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXi >= 1.1.3-i486-1slp
libXinerama >= 1.0.3-i486-1slp
libXrandr >= 1.2.3-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libdrm >= 2.4.1-i486-1slp
libnotify >= 0.4.5-i486-1slp
libpng >= 1.2.34-i486-1slp
libxcb >= 1.1.91-i486-1slp
libxml2 >= 2.6.32-i486-1slp
libxslt >= 1.1.24-i486-1slp
mesa >= 7.2-i486-1slp
pango >= 1.22.4-i486-1slp
pixman >= 0.12.0-i486-1slp
startup-notification >= 0.9-i486-1slp
zlib >= 1.2.3-i486-2
