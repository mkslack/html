GConf (GNOME Configuration database system)

GConf is a configuration database system, functionally similar to
the Windows registry but lots better.

It's being written for the GNOME desktop but does not require GNOME;
configure should notice if GNOME is not installed and compile the
basic GConf library anyway.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.32.0-i486-1slp
bzip2 >= 1.0.6-i486-1
cairo >= 1.10.2-i486-1slp
cxxlibs >= 6.0.14-i486-1 | gcc-g++ >= 4.5.2-i486-2
cyrus-sasl >= 2.1.23-i486-1
dbus >= 1.4.1-i486-1slp
dbus-glib >= 0.92-i486-1slp
expat >= 2.0.1-i486-2
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.8-i486-1slp
gcc >= 4.5.2-i486-2
gdk-pixbuf >= 2.24.1-i486-1slp
glib2 >= 2.28.8-i486-1slp
glibc-solibs >= 2.13-i486-4
gtk+2 >= 2.24.10-i486-1slp
libX11 >= 1.4.4-i486-1slp
libXau >= 1.0.6-i486-1slp
libXcomposite >= 0.4.3-i486-1slp
libXcursor >= 1.1.12-i486-1slp
libXdamage >= 1.1.3-i486-1slp
libXdmcp >= 1.1.0-i486-1slp
libXext >= 1.3.0-i486-1slp
libXfixes >= 5.0-i486-1slp
libXi >= 1.5.0-i486-1slp
libXinerama >= 1.1.1-i486-1slp
libXrandr >= 1.3.2-i486-1slp
libXrender >= 0.9.6-i486-1slp
libpng >= 1.4.9-i486-1slp
libxcb >= 1.7-i486-1slp
libxml2 >= 2.7.8-i486-3
openldap-client >= 2.4.23-i486-1
openssl >= 0.9.8r-i486-3 | openssl-solibs >= 0.9.8r-i486-3
orbit2 >= 2.14.19-i486-1slp
pango >= 1.28.4-i486-1slp
pixman >= 0.24.0-i486-1slp
polkit >= 0.102-i486-1slp
python >= 2.6.6-i486-1
zlib >= 1.2.5-i486-4
