kdebase-runtime (parts of the KDE base runtime module)

This package contains core components of the KDE base 
runtime module.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
alsa-lib >= 1.0.18-i486-2
attr >= 2.4.43_1-i486-1
bzip2 >= 1.0.5-i486-1
clucene >= 0.9.21b-i486-1slp
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
cyrus-sasl >= 2.1.23-i486-1
dbus >= 1.2.14-i486-1slp
e2fsprogs >= 1.41.8-i486-1
expat >= 2.0.1-i486-1
flac >= 1.2.1-i486-1slp
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
gamin >= 0.1.10-i486-1slp
gcc >= 4.3.3-i486-3
gdbm >= 1.8.3-i486-4
glib2 >= 2.22.2-i486-1slp
glibc-solibs >= 2.9-i486-3
ilmbase >= 1.0.1-i486-1slp
kdelibs >= 4.3.2-i486-1slp
kdelibs-experimental >= 4.3.2-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXpm >= 3.5.7-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXtst >= 1.0.3-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libcap >= 2.16-i486-2
libdrm >= 2.4.13-i486-1slp
libjpeg >= 6b-i486-5
libogg >= 1.1.4-i486-1slp
libpng >= 1.2.40-i486-1slp
libsndfile >= 1.0.20-i486-1slp
libungif >= 4.1.4-i486-4
libvorbis >= 1.2.2-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.3-i486-1slp
mesa >= 7.5.1-i486-1slp
openexr >= 1.6.1-i486-1slp
openldap-client >= 2.3.43-i486-1slp
openssl >= 0.9.8k-i486-2 | openssl-solibs >= 0.9.8k-i486-2
pcre >= 7.9-i486-1slp
perl >= 5.10.0-i486-1
phonon >= 4.3.1-i486-1slp
pulseaudio >= 0.9.19-i486-1slp
qt4 >= 4.5.2-i486-1slp
samba >= 3.2.13-i486-1
soprano >= 2.3.0-i486-1slp
sqlite >= 3.6.17-i486-1slp
strigi >= 0.7.0-i486-1slp
utempter >= 1.1.4-i486-1
xine-lib >= 1.1.16.3-i686-1slp
xz >= 4.999.8beta-i486-1
zlib >= 1.2.3-i486-2
