emodules-comp-scale (extra module for enlightenemt)

comp-scale is a module to scale composite windows.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2 >= 1.0.6-x86_64-1
curl >= 7.29.0-x86_64-3_slack14.0
cyrus-sasl >= 2.1.23-x86_64-4
dbus >= 1.4.20-x86_64-4_slack14.0
e_dbus >= 1.7.8-x86_64-1slp
ecore >= 1.7.8-x86_64-1slp
edje >= 1.7.8-x86_64-1slp
eet >= 1.7.8-x86_64-1slp
eeze >= 1.7.8-x86_64-1slp
efreet >= 1.7.8-x86_64-1slp
eina >= 1.7.8-x86_64-1slp
eio >= 1.7.8-x86_64-1slp
elementary >= 1.7.8-x86_64-1slp
embryo >= 1.7.8-x86_64-1slp
emotion >= 1.7.8-x86_64-1slp
evas >= 1.7.8-x86_64-1slp
expat >= 2.0.1-x86_64-2
fontconfig >= 2.9.0-x86_64-1slp
freetype >= 2.4.12-x86_64-1slp
fribidi >= 0.19.5-x86_64-1slp
glib2 >= 2.34.3-x86_64-1slp
glibc-solibs >= 2.15-x86_64-8_slack14.0
gmp >= 5.0.5-x86_64-1
gnutls >= 3.0.32-x86_64-1slp
libX11 >= 1.6.2-x86_64-1slp
libXScrnSaver >= 1.2.2-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXcomposite >= 0.4.4-x86_64-1slp
libXcursor >= 1.1.14-x86_64-1slp
libXdamage >= 1.1.4-x86_64-1slp
libXdmcp >= 1.1.1-x86_64-1slp
libXext >= 1.3.2-x86_64-1slp
libXfixes >= 5.0.1-x86_64-1slp
libXi >= 1.7.2-x86_64-1slp
libXinerama >= 1.1.3-x86_64-1slp
libXp >= 1.0.2-x86_64-1slp
libXrandr >= 1.4.2-x86_64-1slp
libXrender >= 0.9.8-x86_64-1slp
libXtst >= 1.2.2-x86_64-1slp
libgcrypt >= 1.5.3-x86_64-1_slack14.0
libgpg-error >= 1.10-x86_64-1
libidn >= 1.25-x86_64-2
libjpeg >= 8d-x86_64-1slp
libxcb >= 1.9.1-x86_64-1slp
nettle >= 2.6-x86_64-1slp
openldap-client >= 2.4.31-x86_64-2
openssl >= 1.0.1e-x86_64-1_slack14.0 | openssl-solibs >= 1.0.1e-x86_64-1_slack14.0
p11-kit >= 0.12-x86_64-1
udev >= 182-x86_64-5
util-linux >= 2.21.2-x86_64-5
zlib >= 1.2.6-x86_64-1
