xlsclients (utility for listing information about applications)

XlsClients is a utility for listing information about the client
applications running on a X11 server.

Version 1.1 and later of xlsclients use (and require) libxcb instead
of libX11, for more efficient communication with the X server.

xlsclients is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libxcb
