libmsn (implementation of the MSN Messenger service protocol)

Libmsn is a reusable, open-source, fully documented library for
connecting to Microsoft's MSN Messenger service.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
gcc >= 4.3.3-i486-3
glibc-solibs >= 2.9-i486-3
openssl >= 0.9.8k-i486-3_slack13.0 | openssl-solibs >= 0.9.8k-i486-3_slack13.0
