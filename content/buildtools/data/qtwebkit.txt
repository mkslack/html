qtwebkit (render webpages and execute JavaScript code)

The QtWebKit module provides the WebView API, which allows QML
applications to render regions of dynamic web content. A WebView
component may share the screen with other QML components or
encompass the full screen as specified within the QML application.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
eudev >= 3.1.5-x86_64-8
fontconfig >= 2.12.0-x86_64-1slp
freetype >= 2.6.5-x86_64-1slp
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glib2 >= 2.48.2-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
gst-plugins-base1 >= 1.10.4-x86_64-1slp
gstreamer1 >= 1.10.4-x86_64-1slp
libX11 >= 1.6.5-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXrender >= 0.9.10-x86_64-1slp
libjpeg-turbo >= 1.5.0-x86_64-1
libpng >= 1.6.23-x86_64-1
libxml2 >= 2.9.4-x86_64-2
libxslt >= 1.1.29-x86_64-1
mesa >= 17.0.1-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
sqlite >= 3.17.0-x86_64-1slp
xz >= 5.2.2-x86_64-1
zlib >= 1.2.8-x86_64-1
