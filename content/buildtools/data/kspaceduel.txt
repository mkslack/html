kspaceduel (SpaceWar arcade game)

This package is part of the KDE applications collection.

In KSpaceDuel each of two possible players control a satellite
spaceship orbiting the sun. As the game progresses players have to
eliminate the opponent's spacecraft with bullets or mines.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
kauth
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
kcrash
kdbusaddons
ki18n
kwidgetsaddons
kxmlgui
libkdegames
qt5-base
qt5-declarative
qt5-svg
