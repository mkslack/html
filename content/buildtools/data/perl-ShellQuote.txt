perl-ShellQuote (PERL functions for quoting strings)

This distribution contains the String::ShellQuote module, plus a
command-line interface to it.  It contains some functions which are
useful for quoting strings which are going to pass through the shell
or a shell-like object.  It is useful for doing robust tool
programming, particularly when dealing with files whose names
contain white space or shell globbing characters.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
perl >= 5.10.0-i486-1
