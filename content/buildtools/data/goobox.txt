GooBox (A CD player and ripper for GTK+)

Goobox is a CD player and ripper that always
knowns just what to do.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
atk >= 1.28.0-i486-1slp
brasero >= 2.29.91-i486-1slp
cairo >= 1.8.8-i486-1slp
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
dbus >= 1.2.14-i486-1slp
dbus-glib >= 0.80-i486-1slp
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.11-i486-1slp
gcc >= 4.3.3-i486-3
gconf >= 2.26.2-i486-1slp
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
gstreamer >= 0.10.26-i486-1slp
gtk+2 >= 2.18.7-i486-1slp
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXcomposite >= 0.4.1-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdamage >= 1.1.2-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.1-i486-1slp
libXfixes >= 4.0.4-i486-1slp
libXi >= 1.3-i486-1slp
libXinerama >= 1.1-i486-1slp
libXrandr >= 1.3.0-i486-1slp
libXrender >= 0.9.5-i486-1slp
libmusicbrainz2 >= 2.1.5-i486-1slp
libnotify >= 0.4.5-i486-1slp
libpng >= 1.2.42-i486-1slp
libunique >= 1.1.2-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.6-i486-1slp
orbit2 >= 2.14.17-i486-1slp
pango >= 1.26.2-i486-1slp
pixman >= 0.17.6-i486-1slp
zlib >= 1.2.3-i486-2
