gst-plugins-bad (Plugins for GStreamer 1.x)

This is gst-plugins-bad, a set of multimedia plugins for GStreamer.
(Set of plugins that need more quality)



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2
cairo
chromaprint
curl
faac
faad2
fftw
fluidsynth
gcc
gcc-g++
gdk-pixbuf2
glib2
glibc-solibs
gst-plugins-base1
gstreamer1
ilmbase
lcms2
libX11
libdrm
libdvdnav
libdvdread
libgudev
libofa
librsvg
libsndfile
libssh2
libusb
libvdpau
libwebp
libxcb
libxml2
mjpegtools
nettle
openal-soft
openexr
openjpeg2
openssl-solibs
opus
orc
pango
sbc
vulkansdk-loader
x265
