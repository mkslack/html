xfce4-taskmanager (task manager for the Xfce4 desktop)

A simple task manager for the Xfce4 desktop.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
atk
cairo
gdk-pixbuf2
glib2
glibc-solibs
gtk+3
libICE
libSM
libX11
libXmu
libXt
libwnck
pango
zlib
