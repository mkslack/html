kde4games (games for the K Desktop Environment 4.x)

Included with this package are: 
bovo, katomic, kbattleship, kblackbox, kblocks, kbounce, kbreakout,
kdiamond, kfourinline, kfourinlineproc, kgoldrunner, kiriki,
kjumpingcube, klines, kmahjongg, kmines, knetwalk, kolf, kollision,
konquest, kpat, kreversi, ksame, kshisen, ksirk, kspaceduel,
ksquares, ksudoku, ktuberling, kubrick and lskat.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.47_1-i486-1
attr >= 2.4.43_1-i486-1
bzip2 >= 1.0.5-i486-1
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.9-i486-1slp
gamin >= 0.1.10-i486-1slp
gcc >= 4.3.3-i486-3
glib2 >= 2.20.4-i486-1slp
glibc-solibs >= 2.9-i486-3
kde4libs >= 4.3.0-i486-1slp
libICE >= 1.0.5-i486-1slp
libSM >= 1.1.0-i486-1slp
libX11 >= 1.2.2-i486-1slp
libXau >= 1.0.4-i486-1slp
libXcursor >= 1.1.9-i486-1slp
libXdamage >= 1.1.1-i486-1slp
libXdmcp >= 1.0.2-i486-1slp
libXext >= 1.0.5-i486-1slp
libXfixes >= 4.0.3-i486-1slp
libXft >= 2.1.13-i486-1slp
libXpm >= 3.5.7-i486-1slp
libXrender >= 0.9.4-i486-1slp
libXtst >= 1.0.3-i486-1slp
libXxf86vm >= 1.0.2-i486-1slp
libdrm >= 2.4.12-i486-1slp
libpng >= 1.2.38-i486-1slp
libxcb >= 1.3-i486-1slp
libxml2 >= 2.7.3-i486-1slp
mesa >= 7.5-i486-1slp
phonon >= 4.3.1-i486-1slp
qca2 >= 2.0.2-i486-1slp
qt4 >= 4.5.2-i486-1slp
strigi >= 0.7.0-i486-1slp
utempter >= 1.1.4-i486-1
xz >= 4.999.8beta-i486-1
zlib >= 1.2.3-i486-2
