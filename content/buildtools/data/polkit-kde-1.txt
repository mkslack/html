polkit-kde-1 (KDE polkit authenticator)

This is a polkit authenticator for KDE.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
bzip2 >= 1.0.5-i486-1
cxxlibs >= 6.0.13-i486-2 | gcc-g++ >= 4.4.4-i486-1
dbus >= 1.2.24-i486-2
dbus-glib >= 0.86-i486-1slp
eggdbus >= 0.6-i486-1
expat >= 2.0.1-i486-1
fontconfig >= 2.8.0-i486-1slp
freetype >= 2.4.2-i486-1slp
gamin >= 0.1.10-i486-2
gcc >= 4.4.4-i486-1
glib2 >= 2.26.0-i486-1slp
glibc-solibs >= 2.11.1-i486-3
kdelibs >= 4.5.1-i486-1slp
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.99.901-i486-1slp
libXau >= 1.0.6-i486-1slp
libXcursor >= 1.1.10-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.2-i486-1slp
libXfixes >= 4.0.5-i486-1slp
libXft >= 2.1.14-i486-1slp
libXi >= 1.3.2-i486-1slp
libXpm >= 3.5.8-i486-1slp
libXrender >= 0.9.6-i486-1slp
libXtst >= 1.1.0-i486-1slp
libdbusmenu-qt >= 0.5.1-i486-2slp
libpng >= 1.4.3-i486-1slp
libxcb >= 1.6-i486-1slp
mesa >= 7.8.2-i486-1slp
polkit >= 1_14bdfd8-i486-1
polkit-qt-1 >= r1150958-i486-1slp
qt4 >= 4.6.3-i486-1slp
util-linux-ng >= 2.17.2-i486-1
xz >= 4.999.9beta-i486-1
zlib >= 1.2.3-i486-2
