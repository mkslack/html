libarchive (archive reading library - legacy version 2.x)

Libarchive is a programming library that can create and read several
different streaming archive formats, including most popular TAR
variants and several CPIO formats. It can also write SHAR archives.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
acl >= 2.2.51-x86_64-1
attr >= 2.4.46-x86_64-1
bzip2 >= 1.0.6-x86_64-1
glibc-solibs >= 2.15-x86_64-8_slack14.0
libxml2 >= 2.8.0-x86_64-2_slack14.0
openssl >= 1.0.1f-x86_64-1_slack14.0 | openssl-solibs >= 1.0.1f-x86_64-1_slack14.0
xz >= 5.0.4-x86_64-1
zlib >= 1.2.6-x86_64-1
