mkfontscale (Creates an index of scalable font files for X)

mkfontscale creates the fonts.scale and fonts.dir index files used
by the legacy X11 font system.

This package also includes a mkfontdir wrapper script that calls
mkfontscale with appropriate options.

mkfontscale is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
freetype
glibc-solibs
libfontenc
zlib
