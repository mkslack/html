VideoCD (mastering and ripping tool)

VCDImager allows you to create VideoCD BIN/CUE CD images from mpeg
files which can be burned with cdrdao or any other program capable of
burning BIN/CUE files.

VCDRip, which comes with VCDImager, does the reverse operation. That
is, ripping mpeg streams from images (and already burned VideoCDs)
and showing some information about the VideoCD.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libcdio
libxml2
popt
