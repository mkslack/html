kf5-kleopatra (a certificate manager and a universal crypto GUI)

This package is part of the KDE applications collection.

Kleopatra is a certificate manager and a universal crypto GUI. It
supports managing X.509 and OpenPGP certificates in the GpgSM
keybox and retrieving certificates from LDAP servers.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
gpgme >= 1.6.0-x86_64-1
kf5-gpgmepp >= 16.08.1-x86_64-1slp
kf5-kauth >= 5.24.0-x86_64-1slp
kf5-kcmutils >= 5.24.0-x86_64-1slp
kf5-kcodecs >= 5.24.0-x86_64-1slp
kf5-kconfig >= 5.24.0-x86_64-1slp
kf5-kconfigwidgets >= 5.24.0-x86_64-1slp
kf5-kcoreaddons >= 5.24.0-x86_64-1slp
kf5-kdbusaddons >= 5.24.0-x86_64-1slp
kf5-ki18n >= 5.24.0-x86_64-1slp
kf5-kiconthemes >= 5.24.0-x86_64-1slp
kf5-kmime >= 16.08.1-x86_64-1slp
kf5-knotifications >= 5.24.0-x86_64-1slp
kf5-kservice >= 5.24.0-x86_64-1slp
kf5-ktextwidgets >= 5.24.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.24.0-x86_64-1slp
kf5-kwindowsystem >= 5.24.0-x86_64-1slp
kf5-kxmlgui >= 5.24.0-x86_64-1slp
kf5-libkleo >= 16.08.1-x86_64-1slp
kf5-sonnet >= 5.24.0-x86_64-1slp
libassuan >= 2.4.2-x86_64-1
libgpg-error >= 1.23-x86_64-1
qt5-base >= 5.7.0-x86_64-1slp
