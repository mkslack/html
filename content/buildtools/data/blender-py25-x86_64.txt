blender (3D content creation suite)

Blender is an open source software for 3D modeling, animation,
rendering, post-production, interactive creation and playback.

This package requires Python 2.5.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cxxlibs >= 6.0.9-x86_64-1 | gcc-g++ >= 4.2.4-x86_64-1
gcc >= 4.2.4-x86_64-1
glibc-solibs >= 2.7-x86_64-16
libX11 >= 1.1.5-x86_64-1slp
libXau >= 1.0.4-x86_64-1slp
libXdamage >= 1.1.1-x86_64-1slp
libXdmcp >= 1.0.2-x86_64-1slp
libXext >= 1.0.4-x86_64-1slp
libXfixes >= 4.0.3-x86_64-1slp
libXi >= 1.1.3-x86_64-1slp
libXrandr >= 1.2.3-x86_64-1slp
libXrender >= 0.9.4-x86_64-1slp
libXxf86vm >= 1.0.2-x86_64-1slp
libdrm >= 2.4.1-x86_64-1slp
libxcb >= 1.1.91-x86_64-1slp
mesa >= 7.2-x86_64-1slp
python >= 2.5.2-x86_64-3
sdl >= 1.2.13-x86_64-2
svgalib >= 1.9.25-x86_64-1
