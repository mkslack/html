ksmtp (a job-based library to send email through an SMTP server)

This package is part of the KDE applications collection.

ksmtp is a job-based library to send email through an SMTP server.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
cyrus-sasl
gcc
gcc-g++
glibc-solibs
kconfig
kcoreaddons
ki18n
kio
kservice
qt5-base
