perl-AnyEvent (Perl framework to handle multiple event loops)

AnyEvent provides an identical interface to multiple event loops.
This allows module authors to utilise an event loop without forcing
module users to use the same event loop (as only a single event loop
can coexist peacefully at any one time).



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
(No dependicies found)
