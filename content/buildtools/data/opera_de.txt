Opera (internet application suite)

Opera is a free, lightweight Qt internet application suite
containing a browser, and clients for email, RSS, and IRC.

http://www.opera.com/



This is the german 'QT4' opera package.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
expat >= 2.0.1-i486-1
fontconfig >= 2.6.0-i486-1slp
freetype >= 2.3.11-i486-1slp
gcc >= 4.3.3-i486-3
glib2 >= 2.22.4-i486-1slp
glibc-solibs >= 2.9-i486-3
libICE >= 1.0.6-i486-1slp
libSM >= 1.1.1-i486-1slp
libX11 >= 1.3.3-i486-1slp
libXau >= 1.0.5-i486-1slp
libXdmcp >= 1.0.3-i486-1slp
libXext >= 1.1.1-i486-1slp
libXrender >= 0.9.5-i486-1slp
libXt >= 1.0.7-i486-1slp
libpng >= 1.2.42-i486-1slp
libxcb >= 1.3-i486-1slp
qt4 >= 4.6.2-i486-1slp
zlib >= 1.2.3-i486-2
