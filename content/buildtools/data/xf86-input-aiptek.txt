xf86-input-aiptek (Part of the X.Org X server)

xf86-input-aiptek is part of X11.

For more information about the X.Org Foundation (the providers of the
X.Org implementation of the X Window System), see their website:

http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.22-x86_64-4
