xf86-video-voodoo (X.org Voodoo 1 and Voodoo 2 series video driver)

This driver supports Voodoo 1 and Voodoo 2 series video adapters.

xf86-video-voodoo is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
