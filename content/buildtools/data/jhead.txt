jhead (a tool for handling EXIF metadata in JPEG image files)

This package provides a tool for displaying and manipulating
non-image portions of EXIF format JPEG image files, as produced by
most digital cameras.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs >= 2.23-x86_64-1
