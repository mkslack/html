kf5-milou (dedicated search plasmoid for KDE)

This package is part of the KDE Plasma 5 desktop series.

Milou contains a dedicated search application built on top of Baloo.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
kf5-kconfig >= 5.26.0-x86_64-1slp
kf5-kcoreaddons >= 5.26.0-x86_64-1slp
kf5-kpackage >= 5.26.0-x86_64-1slp
kf5-krunner >= 5.26.0-x86_64-1slp
kf5-kservice >= 5.26.0-x86_64-1slp
kf5-plasma-framework >= 5.26.0-x86_64-1slp
qt5-base >= 5.7.0-x86_64-1slp
qt5-declarative >= 5.7.0-x86_64-1slp
