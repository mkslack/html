xf86-video-omap (X.org driver for TI OMAP graphics)

Open-source X.org graphics driver for TI OMAP graphics.

xf86-video-omap is part of X.org. Home: http://www.x.org



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
glibc-solibs
libdrm
