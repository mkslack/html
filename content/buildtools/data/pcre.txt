pcre (Perl-compatible regular expression library)

The PCRE library is a set of functions that implement regular
expression pattern matching using the same syntax and semantics as
Perl 5, with just a few differences (documented in the man page).

The PCRE library is used by KDE's Konqueror browser.



======================================================================
Suggested packages by RequiredBuilder:
(Note: This list may not match the real dependencies!)
======================================================================
cxxlibs >= 6.0.10-i486-1 | gcc-g++ >= 4.3.3-i486-3
gcc >= 4.3.3-i486-3
glibc-solibs >= 2.9-i486-3
