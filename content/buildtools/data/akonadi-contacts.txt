akonadi-contacts (contacts integration for akonadi)

This package is part of the KDE applications collection.

Akonadi is a PIM layer, which provides an asynchronous API to access
all kind of PIM data (e.g. mails, contacts, events, todos etc.).

It consists of several processes (generally called the Akonadi
server) and a library (called client library) which encapsulates the
communication between the client and the server.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
gcc
gcc-g++
glibc-solibs
kauth
kcodecs
kcompletion
kconfig
kconfigwidgets
kcontacts
kcoreaddons
ki18n
kiconthemes
kio
kitemmodels
kjobwidgets
kmime
kservice
ktextwidgets
kwidgetsaddons
kwindowsystem
kxmlgui
prison
qt5-base
sonnet
