kde-baseapps (base applications from the official KDE release)

This package is part of the KDE applications collection.

KDEBaseApps includes base applications from the official KDE release
like dolphin, konqueror, find, folderview-widget.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glib2 >= 2.48.1-x86_64-1slp
glibc-solibs >= 2.23-x86_64-1
kf5-kdelibs >= 4.14.22-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.3-x86_64-1slp
libXau >= 1.0.8-x86_64-1slp
libXdmcp >= 1.1.2-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libXft >= 2.3.2-x86_64-1slp
libXpm >= 3.5.11-x86_64-1slp
libXrender >= 0.9.9-x86_64-1slp
libXt >= 1.1.5-x86_64-1slp
phonon >= 4.9.0-x86_64-1slp
qt >= 4.8.7-x86_64-1slp
tidy-html5 >= 5.3.5git160701-x86_64-1slp
zlib >= 1.2.8-x86_64-1
