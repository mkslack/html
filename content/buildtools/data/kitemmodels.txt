kitemmodels (additional item/view models for Qt Itemview)

This package is part of the KDE Frameworks 5 series.

KItemModels contains a library that provides a set of item models
extending the Qt model-view framework.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glibc-solibs
qt5-base
qt5-declarative
