flickcurl (a C library for the Flickr API)

Flickcurl is a C library for the Flickr API, handling creating the
requests, signing, token management, calling the API, marshalling
request parameters and decoding responses. It uses libcurl to call
the REST web service and libxml2 to manipulate the XML responses.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
curl >= 7.49.1-x86_64-1
cyrus-sasl >= 2.1.26-x86_64-1
glibc-solibs >= 2.23-x86_64-1
libidn >= 1.30-x86_64-1
libssh2 >= 1.7.0-x86_64-1
libxml2 >= 2.9.4-x86_64-2
openldap-client >= 2.4.42-x86_64-1
openssl-solibs >= 1.0.2h-x86_64-1
xz >= 5.2.2-x86_64-1
zlib >= 1.2.8-x86_64-1
