kmailtransport (a mail transport service)

This package is part of the KDE applications collection.

kmailtransport is a mail transport service for KDE.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
akonadi
akonadi-mime
gcc
gcc-g++
glibc-solibs
kauth
kcmutils
kcodecs
kcompletion
kconfig
kconfigwidgets
kcoreaddons
ki18n
kio
kitemmodels
kjobwidgets
kmime
kservice
ksmtp
kwallet
kwidgetsaddons
kwindowsystem
libkgapi
qt5-base
