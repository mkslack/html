kf5-kinfocenter (overview of your system library)

This package is part of the KDE Plasma 5 desktop series.

KInfoCenter contains an application that provides information
about a computer system.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc >= 5.3.0-x86_64-3
gcc-g++ >= 5.3.0-x86_64-3
glibc-solibs >= 2.23-x86_64-1
glu >= 9.0.0-x86_64-1slp
kf5-kauth >= 5.26.0-x86_64-1slp
kf5-kbookmarks >= 5.26.0-x86_64-1slp
kf5-kcmutils >= 5.26.0-x86_64-1slp
kf5-kcodecs >= 5.26.0-x86_64-1slp
kf5-kcompletion >= 5.26.0-x86_64-1slp
kf5-kconfig >= 5.26.0-x86_64-1slp
kf5-kconfigwidgets >= 5.26.0-x86_64-1slp
kf5-kcoreaddons >= 5.26.0-x86_64-1slp
kf5-kcrash >= 5.26.0-x86_64-1slp
kf5-kdbusaddons >= 5.26.0-x86_64-1slp
kf5-kdeclarative >= 5.26.0-x86_64-1slp
kf5-kdelibs4support >= 5.26.0-x86_64-1slp
kf5-kguiaddons >= 5.26.0-x86_64-1slp
kf5-ki18n >= 5.26.0-x86_64-1slp
kf5-kiconthemes >= 5.26.0-x86_64-1slp
kf5-kio >= 5.26.0-x86_64-1slp
kf5-kitemviews >= 5.26.0-x86_64-1slp
kf5-kjobwidgets >= 5.26.0-x86_64-1slp
kf5-knotifications >= 5.26.0-x86_64-1slp
kf5-kparts >= 5.26.0-x86_64-1slp
kf5-kservice >= 5.26.0-x86_64-1slp
kf5-ktextwidgets >= 5.26.0-x86_64-1slp
kf5-kunitconversion >= 5.26.0-x86_64-1slp
kf5-kwayland >= 5.26.0-x86_64-1slp
kf5-kwidgetsaddons >= 5.26.0-x86_64-1slp
kf5-kwindowsystem >= 5.26.0-x86_64-1slp
kf5-kxmlgui >= 5.26.0-x86_64-1slp
kf5-solid >= 5.26.0-x86_64-1slp
kf5-sonnet >= 5.26.0-x86_64-1slp
libICE >= 1.0.9-x86_64-1slp
libSM >= 1.2.2-x86_64-1slp
libX11 >= 1.6.3-x86_64-1slp
libXext >= 1.3.3-x86_64-1slp
libraw1394 >= 2.1.1-x86_64-1
mesa >= 12.0.1-x86_64-1slp
pciutils >= 3.4.1-x86_64-2
qt5-base >= 5.7.0-x86_64-1slp
qt5-declarative >= 5.7.0-x86_64-1slp
zlib >= 1.2.8-x86_64-1
