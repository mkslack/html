pamixer (pulseaudio command line mixer)

pamixer is a console based mixer for pulseaudio.  It can adjust the
volume levels of the audio sinks.

Homepage:  https://github.com/cdemoulins/pamixer



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
boost
gcc
gcc-g++
glibc-solibs
pulseaudio
