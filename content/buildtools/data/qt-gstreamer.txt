qt-gstreamer (C++ bindings for GStreamer)

QtGStreamer is a set of libraries and plugins providing C++ bindings
for GStreamer with a Qt-style API plus some helper classes for
integrating GStreamer better in Qt applications.



======================================================================
Suggested/required packages:
(Note: This list may not match the real dependencies!)
======================================================================
gcc
gcc-g++
glib2
glibc-solibs
gst-plugins-base1
gstreamer1
libglvnd
qt5-base
qt5-declarative
