# pkgBuildBot
pkgBuildBot is a collection of scripts to build/install KDE, XFCE and many other applications from the official source packages designed to work with Slackware Linux.

## About
This repository contains the sources for the project website including all buildsystem and buildtool archives.
For more information about the buildsystem please visit the **[pkgBuildBot](https://gitlab.com/mkslack/pkgBuildBot)** website.
